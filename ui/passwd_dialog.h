/*
 * passwd_dialog.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid-gtk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __PASSWD_DIALOG_H__
#define __PASSWD_DIALOG_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations:
class Args;
class WindowMain;
class SaveDialog;

class PasswdDialog : public Gtk::Dialog
{
public:
  PasswdDialog (const int&, Glib::ustring, const Glib::ustring&, const Glib::ustring&, bool);  
  virtual ~PasswdDialog();

private:
  int &ref_action;
  Glib::ustring &ref_passwd;
  Glib::ustring &ref_wifi_name;
  Glib::ustring essid;
  bool is_encryption;
  
  //Signal handlers:
  //void on_checkbox_editable_toggled();
  void on_checkbox_visibility_toggled();
  
  void on_button_connect ();
  void on_button_show_dialog ();
  //void on_client (const Glib::ustring&, const Args&);
  
  bool on_timeout();
  //void on_checkbutton_activity();
  
  //Child widgets:
#if GTK_MAJOR_VERSION == 2
  Gtk::VBox *m_Box;
#elif GTK_MAJOR_VERSION == 3
  Gtk::Box *m_Box;
#endif
  Gtk::Label        *m_Label;
  Gtk::Entry        *m_Entry;
  Gtk::Button       *m_Button_Connect;
  Gtk::Button       *m_Button_Connect_and_Save;
  Gtk::CheckButton  *m_CheckButton_Visible;
  
  Gtk::ProgressBar m_ProgressBar;

#if GTK_MAJOR_VERSION == 3
  Gtk::Separator m_Separator;
#endif

  Gtk::CheckButton m_CheckButton_Activity;
  
  sigc::slot<void> op_client;
  
  sigc::connection m_connection_timeout;
  bool m_bActivityMode;
};

} // namespace SimpleNetaid

#endif // __PASSWD_DIALOG_H__

