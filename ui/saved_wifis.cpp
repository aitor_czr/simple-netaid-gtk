/*
 * saved_wifis.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid-gtk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <iostream>
#include <glob.h>

#include "window_main.h"
#include "args.h"
#include "functions_ui.h"
#include "saved_wifis.h"
#include <globals.h>
C_LINKAGE_BEGIN
#include <simple-netaid/sbuf.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/netproc.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>
C_LINKAGE_END

namespace SimpleNetaid
{

// Constructor
SavedWifis::SavedWifis(WindowMain *caller, const Args& args)
 : ref(const_cast <Args&>(args))
{
	get_selection()->set_mode( Gtk::SELECTION_SINGLE );
	set_hover_selection( false );
	set_sensitive( true );
	//set_activate_on_single_click( true );
  
	m_refTreeModel = Gtk::ListStore::create( m_Columns );	
	set_model( m_refTreeModel );

	append_column( "    "   , m_Columns.icon );	
	append_column( "NETWORK"  , m_Columns.network );

	fill_treeview();	
	
	signal_connect().connect(sigc::mem_fun(*this, &SavedWifis::on_callback_connect_to_saved_wifi));
	signal_remove().connect(sigc::mem_fun(*this, &SavedWifis::on_callback_remove_saved_wifi));

    f_ui->signal_ui_update_saved_wifis().connect(sigc::mem_fun (*this, &SavedWifis::on_callback_update_saved_wifis));

	show_all_children();
}

// Destructor
SavedWifis::~SavedWifis()
{

}

void SavedWifis::fill_treeview()
{
	glob_t glob_result;
	m_refTreeModel->clear ();

    glob ("/etc/network/wifi/*", GLOB_TILDE, nullptr, &glob_result);
    
    for(unsigned int i=0; i<glob_result.gl_pathc; ++i) {
		char *base = rindex(glob_result.gl_pathv[i], '/') + 1;
		row = *( m_refTreeModel->append() );
		row[ m_Columns.network ] = Glib::ustring(base);
    }
	
	globfree(&glob_result);
}

// Signal Handler
void SavedWifis::on_row_activated(const Gtk::TreeModel::Path& path,
        Gtk::TreeViewColumn* /* column */)
{
  Gtk::TreeModel::iterator iter = m_refTreeModel->get_iter(path);
  selected_addr = (*iter)[ m_Columns.network ];
}

void SavedWifis::on_about_dialog_response(int response_id) 
{

#if GTK_MAJOR_VERSION == 3
  
  set_activate_on_single_click( true );

#endif
  
  set_hover_selection( true );

  std::cout << response_id
	    << ", close="        << Gtk::RESPONSE_CLOSE
        << ", cancel="       << Gtk::RESPONSE_CANCEL
	    << ", delete_event=" << Gtk::RESPONSE_DELETE_EVENT
        << std::endl;

  if((response_id == Gtk::RESPONSE_CLOSE) ||
     (response_id == Gtk::RESPONSE_CANCEL) )
  {
     m_refTreeModel->clear();

#if GTK_MAJOR_VERSION == 3
     
     set_activate_on_single_click( true );  
     set_sensitive( true );

#endif

  }
}

SavedWifis::type_signal_connect SavedWifis::signal_connect()
{
   return m_signal_connect;
}

void SavedWifis::on_connect()
{
   m_signal_connect.emit();
}

void SavedWifis::on_callback_connect_to_saved_wifi()
{
	const char *ifname = iproute();
	
	if (*ifname != 0) {
	   static const char *info_message = _("\nAlready connected to %s\n");	
       GtkWidget *dialog = gtk_message_dialog_new (NULL,
                                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                                   GTK_MESSAGE_WARNING,
                                                   GTK_BUTTONS_OK,
                                                   info_message, ifname);
       gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid info message"));
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
       return;
    }
	
	Glib::RefPtr<Gtk::TreeView::Selection> refTreeSelection = this->get_selection();
	if(refTreeSelection)
	{
		//const Gtk::TreeModel::iterator& iter = refTreeSelection->get_selected();
		Gtk::TreeModel::iterator iter = refTreeSelection->get_selected();
		if(iter)
		{ 
            int pid;
	        sigset_t mask;
            std::vector<std::string> argvproc;
	        Glib::ustring addr, cmd, wpa_config_file;
			
			addr = (*iter)[ this->m_Columns.network ];
			wpa_config_file = "/etc/network/wifi/" + addr;
			ref.get_wireless_connection_cmd (cmd, "", "", wpa_config_file);
	        
	        argvproc.push_back ("/bin/sh");
	        argvproc.push_back ("-c");
	        argvproc.push_back (cmd.c_str());
  
            sigemptyset(&mask);
            sigaddset(&mask, SIGUSR2);
            sigprocmask(SIG_BLOCK, &mask, NULL);

            if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
                fprintf(stderr, "Cannot block SIGUSR2: %s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }

            // Spawn the subprocess with pipes for input and output
            Glib::spawn_async_with_pipes (Glib::get_current_dir(),                                    // Working directory
                                          argvproc,                                                   // Command to execute
                                          Glib::SPAWN_SEARCH_PATH | Glib::SPAWN_DO_NOT_REAP_CHILD,    // Spawn flags
                                          sigc::slot<void>(),                                         // Callback
                                          &pid,                                                       // Process ID
                                          nullptr,                                                    // Standard input channel
                                          nullptr,                                                    // Standard output channel
                                          nullptr                                                     // Standard error channel
                                         );
                                  
            g_child_watch_add(pid, task_cb, (gpointer)&mask);
		}
	}
}

SavedWifis::type_signal_remove SavedWifis::signal_remove()
{
   return m_signal_remove;
}

void SavedWifis::on_remove()
{
   m_signal_remove.emit();
}

void SavedWifis::on_callback_remove_saved_wifi()
{
	Glib::RefPtr<Gtk::TreeView::Selection> refTreeSelection = this->get_selection();
	if(refTreeSelection)
	{
		//const Gtk::TreeModel::iterator& iter = refTreeSelection->get_selected();
		Gtk::TreeModel::iterator iter = refTreeSelection->get_selected();
		if(iter)
		{
			Glib::ustring cmd;
			Glib::RefPtr<Gio::File> ref_path;
			Glib::ustring addr = (*iter)[ this->m_Columns.network ];
			Glib::ustring wpa_config_file = "/etc/network/wifi/" + addr;
			ref.get_uninstall_cmd (cmd, wpa_config_file);		
	        Glib::spawn_command_line_sync (cmd);
	        ref_path = Gio::File::create_for_path(wpa_config_file);
	        if (!ref_path->query_exists())
	            m_refTreeModel->erase(*iter);
		}
	}
}

void SavedWifis::task_cb (int pid, int status, gpointer user_data)
{
    Glib::spawn_close_pid (pid);
  
    if (status != 0) {
       /* error */
       std::cout << "Command failed" << std::endl;
    }
    else {
   	    
   	    sigset_t *mask = (sigset_t *)user_data;
   
        if (sigprocmask(SIG_UNBLOCK, mask, NULL) == -1) {
            fprintf(stderr, "Cannot unblock SIGUSR2: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
		
		sbuf_t s;
		bool connected_ok = false;             
        const char * ifname = iproute();
		if (*ifname != 0) connected_ok = true;
		ui_update_menubar (connected_ok);
		if (connected_ok) 
		   ui_update_toolbar (false);
		else 
		   ui_update_toolbar (true);
			 
		ui_update_statusbar (ifname);
			
        sbuf_init(&s);
        netproc(&s);
		ui_update_status_icon (s.buf);
		sbuf_free(&s);
    }
}

void SavedWifis::on_callback_update_saved_wifis ()
{	
	fill_treeview ();
}

} // namespace SimpleNetaid




	
	
	
	
	
	
	
