 /*
 * main.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <gtkmm.h>
#include <vector>
#include <iostream>
#include <memory>
#include <getopt.h>
#include <stdarg.h>
#include <pwd.h>
#include <pstat/libpstat.h>

//https://github.com/benhoyt/inih
#include <ini.h>  

#include "window_main.h"
#include "args.h"
#include "functions_ui.h"
#include "INIReader.h"
#include <globals.h>

#define MAX_SIZE 1024

#define strdup_or_null(str)  (str) != NULL ? strdup(str) : NULL

const char *progname;

functions_Ui * f_ui;

C_LINKAGE_BEGIN
#include <simple-netaid/sbuf.h>
#include <simple-netaid/helpers.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/iproute.h>
#include <sigact.h>

void get_interfaces(struct sbuf*, struct sbuf*);

functions_Ui* new_functions_ui ()
{
   return new functions_Ui();
}
  
void ui_update ()
{
   return f_ui->do_update ();
}
  
void ui_update_status_icon (const char *str)
{
   return f_ui->do_update_status_icon (str);
}
  
void ui_update_statusbar (const char *str)
{
   return f_ui->do_update_statusbar (str);
}
  
void ui_update_saved_wifis ()
{
   return f_ui->do_update_saved_wifis ();
}
  
void ui_update_menubar (bool connected_ok)
{
   return f_ui->do_update_menubar (connected_ok);
}
  
void ui_update_toolbar (bool wired_ok)
{
   return f_ui->do_update_toolbar (wired_ok);
}
  
void ui_wired_connection ()
{
   return f_ui->do_wired_connection ();
}
  
void ui_wireless_on ()
{
   return f_ui->do_wireless_on ();
}
  
void ui_delete ()
{
   return f_ui->do_delete ();
}
C_LINKAGE_END

static const char *warning_message = "\n\
Another instance of simple-netaid-gtk \n\
is already running.\n";         

static const char *netaid_warning_message = "\n\
Netaid group is not assigned to the current user \n\
account. To add an existing user account to this \n\
group on your system, use the usermod command:\n\n\
 # usermod -aG netaid <username>\n\n\
replacing <username> with the name of the user \n\
you want to add.\n";  

bool is_already_running();

// print usage statement
static 
void config_usage(const char *progname)
{
   fprintf(stderr, "%s -- Simple-netaid-gtk\n\n"
   "Usage: %s [options]\n\n"
      "Options:\n"
      "   -h --help          this help\n"
      "   -c --config        configuration file, default '/etc/simple-netaid/gtk.ini'\n"
      "   -s --systray       run in the systray\n"
      "   -x --xpos          x position\n"
      "   -y --ypos          y position\n"
      "   -W --width         window width\n"
      "   -H --height        window height\n",
      progname, progname);
}

int main (int argc , char **argv)
{
    char pwbuf[1024];
    struct passwd pw, *ppw;
    int errno;
    FILE *pf = NULL;
    char output[256]={0};
    bool found_ok = false;

    char target[PATH_MAX]={0};
    Glib::ustring config_file;

    char *buf = nullptr;
    bool m_systray = false;
    int xpos = -1;
    int ypos = -1;
    int width = 850;
    int height = 700;
   
    errno = getpwuid_r(getuid(), &pw, pwbuf, sizeof(pwbuf), &ppw);
    if(errno) return -1;
    else if(!ppw) return 0;
	
    pf = popen("getent group netaid", "r");
    if(!pf) {
       printf("error opening the pipe %s\n", strerror(errno));
       exit(EXIT_FAILURE);
    }
	
    if(fgets(output, sizeof(output), pf)) {
       char *temp = NULL;
       output[strcspn(output, "\n")] = '\0';
       temp = rindex(output, ':') + 1;
	   char delim[2] = ",";
       char *token = strtok(temp, delim);
       while(token != NULL) {
          if(!strcmp(token, ppw->pw_name)) {
             found_ok = true;
             break;
          } 
          token = strtok(NULL, delim);
	   }
    }
    pclose(pf);

    if(!found_ok) {
       GtkWidget *dialog;
       gtk_init(&argc, &argv);
       dialog = gtk_message_dialog_new(NULL,
          GTK_DIALOG_DESTROY_WITH_PARENT,
          GTK_MESSAGE_WARNING,
          GTK_BUTTONS_OK,
          netaid_warning_message);
       gtk_window_set_title(GTK_WINDOW(dialog), "Unallowed operation warning");
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
    }
    
    // Get progname
    // There is a Glib function for this called Glib::get_prgname(), but we 
    // read /proc/PID/exe instead
    std::string filename = "/proc/" + std::to_string(getpid()) + "/exe";
    ssize_t len = ::readlink(filename.c_str(), target, sizeof(target)-1);
    if(len == -1)
       throw std::runtime_error("[ERR]%s: readlink()\n");
    progname = rindex(target, '/') + 1;

    if (is_already_running()) {
       GtkWidget *dialog;
       gtk_init(&argc, &argv);
       dialog = gtk_message_dialog_new(NULL,
          GTK_DIALOG_DESTROY_WITH_PARENT,
          GTK_MESSAGE_WARNING,
          GTK_BUTTONS_OK,
          warning_message);
       gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid warning"));
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
       exit(EXIT_FAILURE);
    }
    
    /* internationalization */
    setlocale(LC_ALL, "");
    bindtextdomain("simple-netaid-gtk", "/usr/share/locale");
    textdomain("simple-netaid-gtk");
    
    static const struct option long_option[] =
    {
        {"config",  required_argument, 0, 'c'},
        {"systray", no_argument,       0, 's'},
        {"xpos",    required_argument, 0, 'x'},
        {"ypos",    required_argument, 0, 'y'},
        {"width",   required_argument, 0, 'W'},
        {"height",  required_argument, 0, 'H'},
        {"help",    no_argument,       0, 'h'},
        {0,         0,                 0,  0 },
    };
        
    buf = (char*)malloc(sizeof(char) * 64);
    if (!buf) {
        fprintf(stderr, _("Memory allocation failure\n"));
        exit (EXIT_FAILURE);
    }
    
    memset(buf, 0, sizeof(buf));
    
    while (1) {
        int c;
        int option_index = 0;

        if ((c = getopt_long(argc, argv, "c:sx:y:W:H:h", long_option, &option_index)) < 0)
            break;
            
        switch (c) {
        
        case 's':
            m_systray = true;
            break;
        case 'c':
            config_file = Glib::ustring(optarg);
            break;
        case 'x':
            xpos = std::stoi(optarg);
            break;
        case 'y':
            ypos = std::stoi(optarg);
            break;
        case 'W':
            width = std::stoi(optarg);
            break;
        case 'H':
            height = std::stoi(optarg);
            break;
        case 'h':
            config_usage(progname);
            return 0;
        case '?':
            if (isprint (optopt))
               fprintf (stderr, _("Unknown option `-%c'.\n"), optopt);
            else
               fprintf (stderr, _("Unknown option character `\\x%x'.\n"),
              optopt);
            return 1;
        default:
            abort ();
            
        } // switch        
    } // while
    
    if(config_file.size() == 0) {
    
        Glib::init();
        
        Glib::RefPtr<Gio::File> ref_path_A = Gio::File::create_for_path("/etc/simple-netaid/gtk.ini");
        Glib::RefPtr<Gio::File> ref_path_B = Gio::File::create_for_path("conf/gtk.ini");
	
	    if ( ref_path_A->query_exists() )
		    config_file = "/etc/simple-netaid/gtk.ini";

	    else if ( ref_path_B->query_exists() )
		    config_file = "conf/gtk.ini";
  
	    else std::cerr << _("Cannot find 'gtk.ini'\n") << std::endl;	
    }        
    
    std::unique_ptr<SimpleNetaid::INIReader> reader(new SimpleNetaid::INIReader(config_file));

    if (reader->ParseError() < 0) {
        std::cout << _("Can't load 'gtk.ini'\n");
        return 1;
    }

	std::unique_ptr<SimpleNetaid::Args> args(new SimpleNetaid::Args(argc, argv, *reader, m_systray, xpos, ypos, width, height));
   
    sigaction_init();	   
    
    f_ui = new functions_Ui();
	
#if GTK_MAJOR_VERSION == 2 

	Gtk::Main kit(argc, argv);
	SimpleNetaid::WindowMain window(*args);
	kit.run(window);
	delete f_ui;
	std::cout << "\033[1m\033[37m" << _("Exit gracefully") << "\033[0m" << std::endl;	
	return EXIT_SUCCESS;

#elif GTK_MAJOR_VERSION == 3

	auto app = Gtk::Application::create();
	SimpleNetaid::WindowMain window(*args);	
	return app->run(window);
		
#endif
}

bool is_already_running()
{
	DIR *dirp = NULL;
	struct dirent *dir = NULL;
	bool is_running = false;
	
	dirp=opendir("/proc");
	if(!dirp) {
		printf("\n\tERROR: unable to open /proc for read\n\n");
		exit(EXIT_FAILURE);
	}
	
	while((dir=readdir(dirp)) != NULL ) {

        int rc = 0;
	    struct stat sb;
        char *proc_cmd = NULL;
        bool is_uint_ok = true;
        std::string pth;
        pid_t pid = getpid();
	    
	    for(unsigned i=0; i<strlen(dir->d_name); i++) {
		   if(!isdigit(dir->d_name[i])) is_uint_ok = false;
	    }
        
        if(!is_uint_ok) continue;       
        
        if(std::stoi(dir->d_name) == (int)pid) continue;
        
        pth = "/proc/" + std::string(dir->d_name);
        if (stat(pth.c_str(), &sb) == -1) {
           perror("stat");
           continue;
        }
        
        if(!lstat(dir->d_name, &sb)) continue;
 
        /* Only consider directories */
        if(!S_ISDIR(sb.st_mode)) continue;

		rc = pstat_get_binary_path_from_string(&proc_cmd, MAX_SIZE, dir->d_name);
		if(rc) continue;

        if(proc_cmd && !strcmp(rindex(proc_cmd, '/') + 1, progname)) {
           is_running = true;
	       free(proc_cmd);
           break;
        }
	    free(proc_cmd);
	}
	
    closedir(dirp);
    
    return is_running;
}
