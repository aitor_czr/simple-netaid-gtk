/*
 * active_wifis.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <iostream>
#include <json/json.h>

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#include "active_wifis.h"
#include "functions_ui.h"
#include "window_main.h"
#include "args.h"
#include "passwd_dialog.h"
#include "buttonbox.h"
#include <globals.h>

C_LINKAGE_BEGIN
#include <simple-netaid/sbuf.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/netproc.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>
C_LINKAGE_END

namespace SimpleNetaid
{

static int stdinput, stdoutput, stderror;
static std::string iwlist_scanning;

// Constructor
ActiveWifis::ActiveWifis(WindowMain *caller, const Args& args)
 : ref(const_cast <Args&>(args))
{   
    get_selection()->set_mode( Gtk::SELECTION_SINGLE );
	set_hover_selection( true );
	set_sensitive( true );
  
	m_refTreeModel = Gtk::ListStore::create( m_Columns );	
	set_model( m_refTreeModel );
	
	append_column( " ESSID"                      , m_Columns.essid );
	append_column( " ADDRESS                   " , m_Columns.address );
	append_column( " KEY"                        , m_Columns.encryption );
	cell = Gtk::manage( new Gtk::CellRendererProgress() );
	cols_count = append_column( " QUALITY             ", *cell );

	g_object_set(cell->gobj(),
               "cell-background", "#eae8e6",
               "cell-background-set", TRUE,
               NULL);    

	signal_button_press_event().connect
	(
		sigc::mem_fun(*this, &ActiveWifis::on_button_press_event),
		false
	);
	
	iwlist_scanning_connector = new sigc::connection
    (
       signal_iwlist_scanning().connect(sigc::mem_fun(*this, &ActiveWifis::on_callback_iwlist_scanning))
    );
    
    caller->property_hidden().signal_changed().connect
    ( 
        sigc::bind<WindowMain*>
        (
            sigc::mem_fun ( *this, &ActiveWifis::on_callback_hidden_changed),
	        caller
        )
    );
    
    //Connect Server signals to the signal handlers in Client.
    update_on_startup_connector = new sigc::connection
    (
        f_ui->signal_ui_update().connect(sigc::mem_fun(*this, &ActiveWifis::on_callback_ui_update_on_startup))
    );
        
    if (!caller->property_hidden())
        update_on_startup_connector->unblock();
    else
        update_on_startup_connector->block();
    
    iwlist_scanning_connector->unblock();

	show_all_children();
}

// Destructor
ActiveWifis::~ActiveWifis()
{
    delete update_on_startup_connector;
    delete iwlist_scanning_connector;
}

void ActiveWifis::on_callback_parse_output()
{
    JSONCPP_STRING err;
    Json::Value root;
    Json::CharReaderBuilder builder;

	std::vector<std::string> essid;
	std::vector<std::string> address;
	std::vector<int> quality;
	std::vector<bool> encryption;
	
	unsigned i=0, j=0; 
	   
	const std::string rawJson = iwlist_scanning; 
	const auto rawJsonLength = static_cast<int>(rawJson.length());
	   
    const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
    if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root, &err)) {
        std::cout << "error" << std::endl;
        goto Finish_on_callback_parse_output;
    }
  
    std::cout << root << std::endl;
	
	if (root.size() == 0) 
	   goto Finish_on_callback_parse_output;

	while (i < root.size()) {
		std::string idx = "Cell ";
		if (i + 1 < 10) idx += "0";
		idx += std::to_string (i + 1);
		i++;
        
        /* Address */
        address.push_back( root[idx]["Address"].asString() );
        
        /* Essid */
        essid.push_back( root[idx]["Essid"].asString() );
        
        /* Encryption key */
        if(!strcmp(root[idx]["Encryption key"].asString().c_str(), "off"))
            encryption.push_back(false);
        else
            encryption.push_back(true);
           
        /* Quality */
        std::vector<std::string> v;
        std::string aux = root[idx]["Quality"].asString();
		size_t found = aux.find("/");
		v.push_back(aux.substr(0, found));
		v.push_back(aux.substr(found+1, aux.size()));
		quality.push_back(std::stoi(v[0])*100/std::stoi(v[1]));
		v.clear();
		v.shrink_to_fit();
    }

	for(i=0; i<quality.size(); i++)
		for(j=0; j<quality.size()-1; j++)
			if(quality[j]<quality[j+1]) {
				
				std::string temp_essid=essid[j];
				essid[j]=essid[j+1];
				essid[j+1]=temp_essid;
						
				std::string temp_address=address[j];
				address[j]=address[j+1];
				address[j+1]=temp_address;
						
				bool temp_encryption=encryption[j];
				encryption[j]=encryption[j+1];
				encryption[j+1]=temp_encryption;
						
				int temp_quality=quality[j];
				quality[j]=quality[j+1];
				quality[j+1]=temp_quality;
			}

	for(i=0; i<essid.size(); i++) {
		row = *(m_refTreeModel->append());
		row[m_Columns.essid] = essid[i];
		row[m_Columns.address] = address[i];
		row[m_Columns.encryption] = encryption[i];
		row[m_Columns.strength] = quality[i];
	}

	pColumn = get_column (cols_count - 1);
	if(pColumn)
       pColumn->add_attribute (cell->property_value(), m_Columns.strength);

	essid.clear();
    essid.shrink_to_fit();
    
	address.clear();
    address.shrink_to_fit();
    
	encryption.clear();
    encryption.shrink_to_fit();
    
	quality.clear();
    quality.shrink_to_fit();

Finish_on_callback_parse_output:

    root.clear();
    iwlist_scanning.clear();
   
    if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) {
        fprintf(stderr, "Cannot unblock SIGUSR2: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    
    iwlist_scanning_connector->unblock();

    auto *parent = dynamic_cast<WindowMain*>(this->get_toplevel());
    if(!parent->property_hidden())
        update_on_startup_connector->unblock();
	
	this->show_all_children();
}

// Signal Handler (single click)
bool ActiveWifis::on_button_press_event(GdkEventButton* event)
{
  bool return_value = false;

  //Call base class, to allow normal handling,
  //such as allowing the row to be selected by the right-click:
  return_value = TreeView::on_button_press_event(event);

  //Then do our custom stuff:
  if( (event->type == GDK_BUTTON_PRESS) && (event->button == 1) )
  {
	Glib::RefPtr<Gtk::TreeView::Selection> refTreeSelection = get_selection();
	if(refTreeSelection)
	{
		Gtk::TreeModel::iterator iter = refTreeSelection->get_selected();
		if(iter)
		{ 
            int pid, action = 0;
            bool encryption;
            std::vector<std::string> argvproc;
	        Glib::ustring addr, essid, cmd, passwd, wifi_name, wpa_config_file;
	        
			auto *parent = dynamic_cast<WindowMain*>(this->get_toplevel()); 
			
			addr = (*iter)[ m_Columns.address ];			
			essid = (*iter)[ m_Columns.essid ];
			encryption = (*iter)[ m_Columns.encryption ];
			passwd_dialog = new PasswdDialog (action, essid, passwd, wifi_name, encryption);
  
			set_hover_selection( false );

#if GTK_MAJOR_VERSION == 3

			set_activate_on_single_click( false );

#endif

			passwd_dialog->signal_response().connect
			(
				sigc::mem_fun(*this, &ActiveWifis::on_about_dialog_response)
			);
  
			//Bring it to the front, in case it was already shown:
			passwd_dialog->present();
            passwd_dialog->run();
	        delete passwd_dialog;
	
	        set_hover_selection( true );
    
            wpa_config_file = "";
            if (wifi_name.size () > 0)
                wpa_config_file += "/etc/network/wifi/" + wifi_name;

            if (action == 0)
                return true;
            else if (action == 1)
                wpa_config_file.clear ();
            
            ref.get_wireless_connection_cmd (cmd, essid, passwd, wpa_config_file);
	        
	        argvproc.push_back ("/bin/sh");
	        argvproc.push_back ("-c");
	        argvproc.push_back (cmd.c_str());
  
            sigemptyset(&mask);
            sigaddset(&mask, SIGUSR2);
            sigprocmask(SIG_BLOCK, &mask, NULL);

            if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
                fprintf(stderr, "Cannot block SIGUSR2: %s.\n", strerror(errno));
                exit(EXIT_FAILURE);
            }

            // Spawn the subprocess with pipes for input and output
            Glib::spawn_async_with_pipes ( Glib::get_current_dir(),                                    // Working directory
                                           argvproc,                                                   // Command to execute
                                           Glib::SPAWN_SEARCH_PATH | Glib::SPAWN_DO_NOT_REAP_CHILD,    // Spawn flags
                                           sigc::slot<void>(),                                         // Callback
                                           &pid,                                                       // Process ID
                                           &stdinput,                                                  // Standard input channel
                                           &stdoutput,                                                 // Standard output channel
                                           &stderror                                                   // Standard error channel
                                         );
                                  
            g_child_watch_add(pid, task_cb, (gpointer)&mask);
		}
	}
  }

  return return_value;
}

void ActiveWifis::on_about_dialog_response(int response_id) 
{
    passwd_dialog->hide();
/*	
	set_hover_selection( true );
	
	std::cout
		<< response_id
		<< ", close="        << Gtk::RESPONSE_CLOSE
		<< ", cancel="       << Gtk::RESPONSE_CANCEL
		<< ", delete_event=" << Gtk::RESPONSE_DELETE_EVENT
		<< std::endl;

    if((response_id == Gtk::RESPONSE_CLOSE) || (response_id == Gtk::RESPONSE_CANCEL))
	    m_refTreeModel->clear();
*/ 
}

ActiveWifis::type_signal_iwlist_scanning ActiveWifis::signal_iwlist_scanning()
{
   return m_signal_iwlist_scanning;
}

void ActiveWifis::on_action_iwlist_scanning()
{
    m_signal_iwlist_scanning.emit();
}

bool ActiveWifis::is_spinner_busy ()
{
	return iwlist_scanning_connector->blocked();
}

void ActiveWifis::on_callback_iwlist_scanning()
{
	if (!iwlist_scanning_connector->blocked())
	    iwlist_scanning_connector->block();
	else
	    return;
	
	m_refTreeModel->clear();
	show_all_children();
  
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR2);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
        fprintf(stderr, "Cannot block SIGUSR2: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
	        
	auto *parent = dynamic_cast<WindowMain*>(this->get_toplevel()); 
	
    Gtk::Dialog *dialog = new Gtk::Dialog();
    
    dialog->set_transient_for (*parent);
    dialog->set_deletable (false);
    dialog->set_decorated (false);
    
    Gtk::Label *lbl =  Gtk::manage(new Gtk::Label("\n  Scanning active wifis... Please wait \n"));
    Gtk::Spinner  *spinner = Gtk::manage(new Gtk::Spinner());   

    dialog->set_title( "Simple-netaid" );
    dialog->set_border_width( 10 );
    dialog->set_default_size ( 230, 20 );
    dialog->set_position( Gtk::WIN_POS_CENTER );

    dialog->get_vbox()->pack_start(*spinner, Gtk::PACK_SHRINK);
    dialog->get_vbox()->pack_start(*lbl, Gtk::PACK_SHRINK);
    
    dialog->signal_show().connect
    ( 
       sigc::bind<Gtk::Dialog*>
       (
          sigc::mem_fun(*this, &ActiveWifis::on_callback_spinner_show),
          dialog
       )
    );
    
    spinner->start();

    dialog->signal_hide().connect(sigc::mem_fun(*this, &ActiveWifis::on_callback_parse_output));

    dialog->show_all_children();
    
    dialog->present();
}

void ActiveWifis::on_callback_spinner_show (Gtk::Window *window)
{
	int pid;
	Glib::ustring cmd;
    std::vector<std::string> argvproc;
	ref.get_iwlist_scanning_cmd (cmd);
 
	argvproc.push_back ("/bin/sh");
	argvproc.push_back ("-c");
	argvproc.push_back (cmd.c_str());

    // Spawn the subprocess with pipes for input and output
    Glib::spawn_async_with_pipes ( Glib::get_current_dir(),                                    // Working directory
                                   argvproc,                                                   // Command to execute
                                   Glib::SPAWN_SEARCH_PATH | Glib::SPAWN_DO_NOT_REAP_CHILD,    // Spawn flags
                                   sigc::slot<void>(),                                         // Callback
                                   &pid,                                                       // Process ID
                                   &stdinput,                                                  // Standard input channel
                                   &stdoutput,                                                 // Standard output channel
                                   &stderror                                                   // Standard error channel
                                 );
                                  
	GtkWindow* _gobj = window->gobj();
    g_child_watch_add(pid, watch_pid, (gpointer)_gobj);
}

void ActiveWifis::on_callback_hidden_changed (WindowMain *caller)
{
	if(!caller->property_hidden()) {
	    on_callback_iwlist_scanning();
	} 
	else {
	    update_on_startup_connector->block();
	}    
}

void ActiveWifis::on_callback_ui_update_on_startup ()
{    
    update_on_startup_connector->block();
	on_callback_iwlist_scanning();
}

void ActiveWifis::replace_all(std::string& str, const std::string& from, const std::string& to)
{
  size_t index = 0;
  while (true) {
    // find substring
    index = str.find(from, index);
    if (index == std::string::npos) break;

    // relace it with target to
    str.replace(index, from.length(), to);

    // jump further to after replacement
    index += to.length(); 
  }
}

void ActiveWifis::task_cb (int pid, int status, gpointer user_data)
{
    Glib::spawn_close_pid (pid);
  
    if (status != 0) {
       /* error */
       std::cout << "Command failed" << std::endl;
    }
    else {
   	    
   	    sigset_t *mask = (sigset_t *)user_data;
   
        if (sigprocmask(SIG_UNBLOCK, mask, NULL) == -1) {
            fprintf(stderr, "Cannot unblock SIGUSR2: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
		
		sbuf_t s;
		bool connected_ok = false;             
        const char * ifname = iproute();
		if (*ifname != 0) connected_ok = true;
		ui_update_menubar (connected_ok);
		if (connected_ok) 
		   ui_update_toolbar (false);
		else 
		   ui_update_toolbar (true);
			 
		ui_update_statusbar (ifname);
			
        sbuf_init(&s);
        netproc(&s);
		ui_update_status_icon (s.buf);
		sbuf_free(&s);

        if(connected_ok) 
            ui_update_saved_wifis ();
    }
}

void ActiveWifis::watch_pid (int pid, int status, gpointer object)
{
    GtkWindow* p = (GtkWindow*)object;

    std::stringstream strm;  
    std::string item;
    iwlist_scanning.clear();
  
    if (status != 0) {
       /* error */
       std::cout << "Command failed" << std::endl;
       /* At this point we're going to slow down how often the periodic reminder sends signals, 
        * i.e. the frecuence of the attempts to fill the active wifis treeview, empty yet */
       union sigval sv;
       sv.sival_int = 5; /* value to be sent */
       /* Have a look at 'static volatile sig_atomic_t keep_updating_active_wifis' in sigact.c */
       if(sigqueue(getpid(), SIGUSR2, sv) != 0) {   
           perror("SIGSENT-ERROR:");
       }
       goto Finish_watch_pid;
    }
    else {
   
       /* success */
        char ch;
        union sigval sv;
        sv.sival_int = -1; // value to be sent (a negative value stops the scanner)
        /* Stop updating the active wifis treeview setting 'active_wifis_interval = -1' in sigact.c */
        if(sigqueue(getpid(), SIGUSR2, sv) != 0) {   
            perror("SIGSENT-ERROR:");
        } 
        std::unique_ptr<FILE, decltype(&pclose)> fp(::fdopen(stdoutput, "rb"), &pclose); 
        while ((ch = (char)fgetc(fp.get()))!=EOF)
            strm << ch;
    } 
 
    iwlist_scanning.append(strm.str());
	
	strm.str(std::string());
    strm.clear();

Finish_watch_pid:

    Glib::spawn_close_pid (pid);
	
#if GTK_MAJOR_VERSION == 2

    gtk_widget_destroy(GTK_WIDGET(p));

#elif GTK_MAJOR_VERSION == 3

    gtk_window_close(p);
   
#endif
}

} // namespace SimpleNetaid

