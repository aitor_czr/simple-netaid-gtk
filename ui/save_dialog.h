/*
 * passwd_dialog.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid-gtk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __SAVE_DIALOG_H__
#define __SAVE_DIALOG_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class myButtonBox;

class SaveDialog : public Gtk::Dialog
{
public:
  SaveDialog(const int&, const Glib::ustring&);  
  virtual ~SaveDialog();

private:
    int &ref_action;
    Glib::ustring &ref_wifi_name;
    
    Gtk::Label  *dLabel;
    Gtk::Entry  *dEntry;
    myButtonBox *dButtonBox;
    Gtk::Button *dButton1, *dButton2;
    
    void on_dialog_connect_and_save ();
    void on_dialog_cancel ();
};

} // namespace SimpleNetaid

#endif // __SAVE_DIALOG_H__

