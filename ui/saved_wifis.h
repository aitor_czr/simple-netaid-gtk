/*
 * saved_wifis.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid-gtk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __SAVED_WIFIS_H__
#define __SAVED_WIFIS_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;

class SavedWifis : public Gtk::TreeView
{
public:
	SavedWifis( WindowMain*, const Args& );
	virtual ~SavedWifis();

   void on_connect();
   void on_remove();

   //signal accessors:
   typedef sigc::signal<void()>              type_signal_connect;
   typedef sigc::signal<void()>              type_signal_remove;
   
   type_signal_connect signal_connect();
   type_signal_remove  signal_remove();

protected:
	class ColModel : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ColModel()
		{			
			add(icon);
			add(network);
		};
   
		Gtk::TreeModelColumn< Glib::RefPtr<Gdk::Pixbuf> >  icon;
		Gtk::TreeModelColumn< Glib::ustring >              network;
	};
	    
	void fill_treeview();
   
    sigc::connection *update_saved_wifis_connector;
    
    type_signal_connect m_signal_connect;
    type_signal_remove  m_signal_remove;
	
	// Signal Handlers
	void on_row_activated(const Gtk::TreeModel::Path& path,
						  Gtk::TreeViewColumn* /* column */);
	void on_about_dialog_response(int response_id);
   
	// Callbacks
	void on_callback_connect_to_saved_wifi();
	void on_callback_remove_saved_wifi();
	
	void on_callback_update_saved_wifis ();
  
    static void task_cb (int, int, gpointer);
  
private:
    Args& ref;
	ColModel m_Columns;  
	GdkEventAny* any_event;
	Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
	Glib::RefPtr<Gtk::TreeSelection> refTreeSelection;		 
	Gtk::TreeModel::Row row;
	Glib::ustring selected_addr;
};

} // namespace SimpleNetaid

#endif // __TREE_VIEW_H__

