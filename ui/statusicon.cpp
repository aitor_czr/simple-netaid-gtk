/*
 * statusicon.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <gtkmm.h>
#include <iostream>

#include "statusicon.h"
#include "window_main.h"
#include "args.h"
#include "popup.h"
#include "functions_ui.h"
#include <globals.h>

namespace SimpleNetaid
{

myStatusIcon::myStatusIcon(WindowMain *caller, const Args& args)
 : ref(const_cast <Args&>(args))
{
	m_PopupMenu = new myPopup(caller, args);
	
	connected_icon = ref.get_connected_icon ();
	disconnected_icon = ref.get_disconnected_icon ();
	
	// Setting up the StatusIcon:
	m_refStatusIcon = Gtk::StatusIcon::create_from_file(ref.get_app_icon ());
	m_refStatusIcon->set_tooltip_text("Disconnected");
	
	update_status_icon_connector = new sigc::connection
    (
       f_ui->signal_ui_update_status_icon().connect(sigc::mem_fun (*this, &myStatusIcon::on_callback_ui_update_status_icon))
    );
    
    update_status_icon_connector->unblock();
  
	// StatusIcon's signals (GTK+)
	GtkStatusIcon* gobj_StatusIcon = m_refStatusIcon->gobj();
	g_signal_connect(G_OBJECT(gobj_StatusIcon), "activate", G_CALLBACK(on_statusicon_activated), caller);
	g_signal_connect(G_OBJECT(gobj_StatusIcon), "popup-menu", G_CALLBACK(on_statusicon_popup), this);
}

myStatusIcon::~myStatusIcon()
{
	if(m_PopupMenu) delete m_PopupMenu;
}

void myStatusIcon::on_callback_ui_update_status_icon(const char *detailed_label)
{
	update_status_icon_connector->block();
	m_refStatusIcon->set_tooltip_text(detailed_label);
	if (!strcmp(detailed_label, "Disconnected"))
	    m_refStatusIcon->set_from_file(disconnected_icon);
	else
	    m_refStatusIcon->set_from_file(connected_icon);
	update_status_icon_connector->unblock();    
}

void myStatusIcon::display_menu(guint button, guint32 activate_time)
{
	// Display the popupmenu:
	m_PopupMenu->show_popup_menu(button, activate_time);
}

/* * 
 *
 *  Gtk+ method:
 *
 */ 

void on_statusicon_activated(GtkWidget* widget, gpointer object)
{
	auto window_main = static_cast<WindowMain*>(object);
	
	if (window_main->is_active_wifis_spinner_busy ()) {
	    return;
	}
   
	window_main->property_hidden() ? window_main->deiconify() : window_main->iconify();	
	window_main->property_hidden() ? window_main->property_hidden()=false : window_main->property_hidden()=true;
}

// This wraps the statusicon signal "popup-menu" and calls WindowMain::show_popup_menu()
void on_statusicon_popup(GtkStatusIcon* status_icon,
						 guint button,
						 guint32 activate_time,
						 gpointer object)
{
	return static_cast<myStatusIcon*>(object)->display_menu(button, activate_time);
}

} // namespace SimpleNetaid
