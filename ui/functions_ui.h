 /*
  * functions_ui.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  

#ifndef __FUNCTIONS_UI_H__
#define __FUNCTIONS_UI_H__

#include <sigc++/sigc++.h>
#include <globals.h>

class functions_Ui {
public:
   functions_Ui ();
   virtual ~functions_Ui ();
   	
   void do_update ();   	
   void do_update_status_icon (const char*);   	
   void do_update_statusbar (const char*);   	
   void do_update_saved_wifis (); 	 	
   void do_update_menubar (bool); 	
   void do_update_toolbar (bool);
   void do_wired_connection();
   void do_wireless_on();
   void do_delete ();

   //signal accessors:
   typedef sigc::signal<void ()> type_signal_ui_update;
   typedef sigc::signal<void (const char*)> type_signal_ui_update_status_icon;
   typedef sigc::signal<void (const char*)> type_signal_ui_update_statusbar;
   typedef sigc::signal<void ()> type_signal_ui_update_saved_wifis;
   typedef sigc::signal<void (bool)> type_signal_ui_update_menubar;
   typedef sigc::signal<void (bool)> type_signal_ui_update_toolbar;
   typedef sigc::signal<void ()> type_signal_ui_wired_connection;
   typedef sigc::signal<void ()> type_signal_ui_wireless_on;
   typedef sigc::signal<void ()> type_signal_ui_delete;
   
   type_signal_ui_update signal_ui_update ();  
   type_signal_ui_update_status_icon signal_ui_update_status_icon ();  
   type_signal_ui_update_statusbar signal_ui_update_statusbar ();  
   type_signal_ui_update_saved_wifis signal_ui_update_saved_wifis ();    
   type_signal_ui_update_menubar signal_ui_update_menubar ();  
   type_signal_ui_update_toolbar signal_ui_update_toolbar ();  
   type_signal_ui_wired_connection signal_ui_wired_connection ();  
   type_signal_ui_wireless_on signal_ui_wireless_on ();
   type_signal_ui_delete signal_ui_delete ();

protected:
   type_signal_ui_update m_signal_ui_update;
   type_signal_ui_update_status_icon m_signal_ui_update_status_icon;
   type_signal_ui_update_statusbar m_signal_ui_update_statusbar;
   type_signal_ui_update_saved_wifis m_signal_ui_update_saved_wifis;
   type_signal_ui_update_menubar m_signal_ui_update_menubar;
   type_signal_ui_update_toolbar m_signal_ui_update_toolbar;
   type_signal_ui_wired_connection m_signal_ui_wired_connection;
   type_signal_ui_wireless_on m_signal_ui_wireless_on;
   type_signal_ui_delete m_signal_ui_delete;
};

#endif  // __FUNCTIONS_UI_H__
