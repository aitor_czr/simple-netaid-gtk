/*
 * active_wifis.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 
 
#ifndef __ACTIVE_WIFIS_H__
#define __ACTIVE_WIFIS_H__

#include <gtkmm.h>
#include <vector>

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;
class PasswdDialog;

class ActiveWifis : public Gtk::TreeView
{
public:
	ActiveWifis(WindowMain*, const Args&);
	virtual ~ActiveWifis();
	
	// Getter:
	bool is_spinner_busy ();
	
    void on_action_iwlist_scanning(); 

    //signal accessors:
    typedef sigc::signal<void()> type_signal_iwlist_scanning;

    type_signal_iwlist_scanning signal_iwlist_scanning();

protected:

	class ColModel : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ColModel()
		{			
			//add(icon);
			add(essid);
			add(address);
			add(encryption);
			add(strength);
		};
   
		// Gtk::TreeModelColumn< Glib::RefPtr<Gdk::Pixbuf> >  icon;
		Gtk::TreeModelColumn<std::string> essid;
		// Gtk::TreeModelColumn<bool>     installed;
		Gtk::TreeModelColumn<std::string> address;
		Gtk::TreeModelColumn<int>         strength;
		Gtk::TreeModelColumn<bool>        encryption;
	};
	
	Args& ref;
	sigset_t mask;
	ColModel m_Columns;
   
    type_signal_iwlist_scanning m_signal_iwlist_scanning;
   
    sigc::connection *update_on_startup_connector;   
    sigc::connection *iwlist_scanning_connector;
    
    void replace_all(std::string&, const std::string&, const std::string&);
    
    // Callbacks
    void on_callback_ui_update_on_startup();
  
    void on_callback_spinner_show (Gtk::Window*);  
    void on_callback_spinner_hide ();
	
	void on_callback_iwlist_scanning ();
    void on_callback_parse_output ();
    
	void on_callback_hidden_changed (WindowMain*);
	
	/* 
	 * This is for the single click event, since 
	 * set_activate_on_single_click( true ) is not available in gtkmm 2.4
	 * */
	virtual bool on_button_press_event(GdkEventButton*) override;
						  
	void on_about_dialog_response(int response_id);
  
    static void task_cb (int, int, gpointer);  
    static void watch_pid (int, int, gpointer);
	
	// Friend fuctions:
	friend void on_statusicon_activated(GtkWidget*, gpointer);
  
private:  
	GdkEventAny* any_event;
	std::string backend_path;
	guint cols_count;
	Glib::RefPtr<Gtk::ListStore> m_refTreeModel;		 
	Gtk::TreeModel::Row row;
	Gtk::TreeViewColumn* pColumn;
	Gtk::CellRendererProgress* cell;
	PasswdDialog *passwd_dialog;
};

} // namespace SimpleNetaid

#endif // __ACTIVE_WIFIS_H__

