/*
 * popup.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __POPUP_H__
#define __POPUP_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;

class myPopup : public Gtk::Window
{    
public:
	myPopup(WindowMain*, const Args&);
	virtual ~myPopup();

protected:
	//Child widgets:
	Gtk::Menu *m_Menu;  
	Gtk::ImageMenuItem *item; 
	Gtk::SeparatorMenuItem *hline;
	Gtk::CheckMenuItem *checkItem;
	
	Gtk::EventBox  *m_EventBox;
	GdkEventButton *gdkEvent;
	
	// Glib:
	Glib::RefPtr<Gtk::UIManager>   m_refUIManager;
	Glib::RefPtr<Gtk::ActionGroup> m_refActionGroup;
	Glib::RefPtr<Gtk::Action>      m_refActionQuit;
	
	// Signal handlers:
	virtual void on_action_quit(WindowMain*);
	virtual void show_popup_menu(guint, guint32);
	
	// Friend class:
	friend class myStatusIcon;
};

} // namespace SimpleNetaid

#endif // __POPUP_H__
