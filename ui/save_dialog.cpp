/*
 * passwd_dialog.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid-gtk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <glob.h>

#include "save_dialog.h"
#include "passwd_dialog.h"
#include "window_main.h"
#include "buttonbox.h"
#include <globals.h>

namespace SimpleNetaid
{

// Constructor
SaveDialog::SaveDialog (const int &action, const Glib::ustring &wifi_name)
 : ref_action(const_cast <int&>(action)),
   ref_wifi_name(const_cast <Glib::ustring&>(wifi_name))
{
    ref_action = 0;   /* Do nothing */
    ref_wifi_name = Glib::ustring("");

	dLabel = Gtk::manage (new Gtk::Label ("Enter a descriptive name for the installed wifi:")); 
	
	dEntry = Gtk::manage (new Gtk::Entry ());
	
	dButton1 = Gtk::manage (new Gtk::Button ());	
	dButton2 = Gtk::manage (new Gtk::Button ());
	
    dButtonBox = new myButtonBox (false, 10, Gtk::BUTTONBOX_START);

    set_size_request(450, 200);
    set_border_width(10);

    dButton1->set_label("Save + Connect");
    dButton2->set_label("Cancel");
    
    dButton1->signal_clicked().connect (sigc::mem_fun (*this, &SaveDialog::on_dialog_connect_and_save));
    dButton2->signal_clicked().connect (sigc::mem_fun (*this, &SaveDialog::on_dialog_cancel));

    //dButtonBox.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    dButtonBox->bbox->add(*dButton1);
    dButtonBox->bbox->add(*dButton2);
    
    this->get_vbox()->pack_start (*dLabel, Gtk::PACK_SHRINK);
    this->get_vbox()->pack_start (*dEntry, Gtk::PACK_SHRINK);
    this->get_vbox()->pack_end (*dButtonBox, Gtk::PACK_SHRINK);
	
	show_all_children ();
}

// Destructor
SaveDialog::~SaveDialog()
{

}

void SaveDialog::on_dialog_connect_and_save()
{ 
    ref_wifi_name = dEntry->get_text();

	if (ref_wifi_name.size () == 0) {
	    ref_action = 0;   /* Connect */
	    static const char *info_message = _("\nThe name for the installed wifi is empty\n");	
        GtkWidget *dialog = gtk_message_dialog_new (NULL,
                                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                                    GTK_MESSAGE_WARNING,
                                                    GTK_BUTTONS_OK,
                                                    info_message);
        gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid info message"));
        gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
        return;
    }
    
    Glib::RefPtr<Gio::File> ref_path = Gio::File::create_for_path("/etc/network/wifi/" + ref_wifi_name);
    if (ref_path->query_exists()) {
	    ref_action = 0;
	    static const char *info_message = _("\nAnother wifi with alias\n\"%s\"\nalready exists in %s.\nPlease, choose another name.");	
        GtkWidget *dialog = gtk_message_dialog_new (NULL,
                                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                                    GTK_MESSAGE_WARNING,
                                                    GTK_BUTTONS_OK,
                                                    info_message, ref_wifi_name.c_str(), "/etc/network/wifi");
        gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid info message"));
        gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
        ref_wifi_name.clear ();
        dEntry->set_text ("");
        return;
    } 
    
    ref_action = 2;
    hide();	
}

void SaveDialog::on_dialog_cancel()
{
    hide ();
}

} // namespace SimpleNetaid
