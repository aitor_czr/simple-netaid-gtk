/*
 * statusbar.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */ 

#include "statusbar.h"
#include "window_main.h"
#include "args.h"
#include "buttonbox.h"
#include "functions_ui.h"
#include <globals.h>

namespace SimpleNetaid
{
	
// Constructor
myStatusBar::myStatusBar(WindowMain *caller, const Args& args)
{
	m_VBox = Gtk::manage(new Gtk::VBox());
	add(*m_VBox);
    m_VBox->set_border_width(5);
    m_Align = Gtk::manage(new Gtk::Alignment());
    m_VBox->add(*m_Align);
	m_HBox = Gtk::manage(new Gtk::HBox());
	m_Align->add(*m_HBox);
    m_Label = Gtk::manage(new Gtk::Label());
    m_HBox->pack_start(*m_Label, Gtk::PACK_SHRINK, 2);

    update_statusbar_connector = new sigc::connection
    (	
	   f_ui->signal_ui_update_statusbar().connect(sigc::mem_fun (*this, &myStatusBar::on_callback_ui_update_statusbar))
	);	
	update_statusbar_connector->unblock();
	show_all_children ();
}

// Destructor (virtual)
myStatusBar::~myStatusBar()
{

}

void myStatusBar::on_callback_ui_update_statusbar(const char * ifname)
{
	update_statusbar_connector->block();
	
	if (*ifname == 0)
        m_Label->set_markup("<span background=\"red\" foreground=\"white\"> Disconnected </span>");
    else	
	    m_Label->set_markup("<span background=\"green\" foreground=\"white\"> Connected to " + Glib::ustring(ifname) + " </span>");
	
	update_statusbar_connector->unblock();   
}

} // namespace SimpleNetaid
