/*
 * icon_factory.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <iostream>

#include "icon_factory.h"
#include "args.h"

namespace SimpleNetaid
{

IconFactory::IconFactory(const Args& args)
 : ref(const_cast <Args&>(args))
{
	Glib::ustring REFRESH_STOCK_ICON = ref.get_view_refresh_icon ();
	Glib::ustring WIRED_STOCK_ICON = ref.get_wired_icon ();
	Glib::ustring WIRELESS_STOCK_ICON = ref.get_wireless_icon ();
	
	Glib::RefPtr<Gtk::IconFactory> factory = Gtk::IconFactory::create();
		
	add_stock_icon(factory, REFRESH_STOCK_ICON, "refresh", "Refresh");
	add_stock_icon(factory, WIRED_STOCK_ICON, "wired connection", "Wired connection");
	add_stock_icon(factory, WIRELESS_STOCK_ICON, "wireless", "Wireless connection");
	
	factory->add_default(); //Add factory to list of factories.
	
	this->REFRESH_STOCK_ICON = Gtk::StockID("refresh");
	this->WIRED_STOCK_ICON = Gtk::StockID("wired connection");
	this->WIRELESS_STOCK_ICON = Gtk::StockID("wireless");
}

IconFactory::~IconFactory()
{
	
}

void IconFactory::add_stock_icon( const Glib::RefPtr<Gtk::IconFactory> &factory,
								  const Glib::ustring& filepath,								  
                                  const Glib::ustring& id,
                                  const Glib::ustring& label )
{	
	Gtk::IconSource source;
	
	try
	{
		//This throws an exception if the file is not found:
		//source.set_pixbuf(Gdk::Pixbuf::create_from_file(filepath));
		const Glib::RefPtr<Gdk::Pixbuf>& pixbuf = Gdk::Pixbuf::create_from_file(filepath.c_str());
		source.set_pixbuf(pixbuf);
	}
	catch(const Glib::Exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	source.set_size(Gtk::ICON_SIZE_SMALL_TOOLBAR);
	source.set_size_wildcarded(); //Icon may be scaled.

#if GTK_MAJOR_VERSION == 2

	Gtk::IconSet icon_set;
	icon_set.add_source(source); //More than one source per set is allowed.
	
#elif GTK_MAJOR_VERSION == 3

	Glib::RefPtr<Gtk::IconSet> icon_set = Gtk::IconSet::create();
	icon_set->add_source(source); //More than one source per set is allowed.
	
#endif	
		
	const Gtk::StockID stock_id(id);
	factory->add(stock_id, icon_set);
	Gtk::Stock::add(Gtk::StockItem(stock_id, label));
}

} // namespace SimpleNetaid
