 /*
  * functions_ui.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  

#include <iostream>
#include "functions_ui.h"

functions_Ui::functions_Ui ()
{
	
}

functions_Ui::~functions_Ui ()
{
	
}

functions_Ui::type_signal_ui_update functions_Ui::signal_ui_update ()
{
   return m_signal_ui_update;
}

void functions_Ui::do_update ()
{
   m_signal_ui_update.emit ();
}

functions_Ui::type_signal_ui_update_status_icon functions_Ui::signal_ui_update_status_icon ()
{
   return m_signal_ui_update_status_icon;
}

void functions_Ui::do_update_status_icon (const char* str)
{
   m_signal_ui_update_status_icon.emit (str);
}

functions_Ui::type_signal_ui_update_statusbar functions_Ui::signal_ui_update_statusbar ()
{
   return m_signal_ui_update_statusbar;
}

void functions_Ui::do_update_statusbar (const char* str)
{
   m_signal_ui_update_statusbar.emit (str);
}

functions_Ui::type_signal_ui_update_saved_wifis functions_Ui::signal_ui_update_saved_wifis ()
{
   return m_signal_ui_update_saved_wifis;
}

void functions_Ui::do_update_saved_wifis ()
{
   m_signal_ui_update_saved_wifis.emit ();
}

functions_Ui::type_signal_ui_update_menubar functions_Ui::signal_ui_update_menubar ()
{
   return m_signal_ui_update_menubar;
}

void functions_Ui::do_update_menubar (bool connected_ok)
{
   m_signal_ui_update_menubar.emit (connected_ok);
}

functions_Ui::type_signal_ui_update_toolbar functions_Ui::signal_ui_update_toolbar ()
{
   return m_signal_ui_update_toolbar;
}

void functions_Ui::do_update_toolbar (bool wired_ok)
{
   m_signal_ui_update_toolbar.emit (wired_ok);
}

functions_Ui::type_signal_ui_wired_connection functions_Ui::signal_ui_wired_connection ()
{
   return m_signal_ui_wired_connection;
}

void functions_Ui::do_wired_connection ()
{
   m_signal_ui_wired_connection.emit ();
}

functions_Ui::type_signal_ui_wireless_on functions_Ui::signal_ui_wireless_on ()
{
   return m_signal_ui_wireless_on;
}

void functions_Ui::do_wireless_on ()
{
   m_signal_ui_wireless_on.emit ();
}

functions_Ui::type_signal_ui_delete functions_Ui::signal_ui_delete ()
{
   return m_signal_ui_delete;
}

void functions_Ui::do_delete ()
{
   m_signal_ui_delete.emit ();
}


