/*
 * window_main.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <gtkmm.h>
#include <filesystem>
#include <iostream>

#include "window_main.h"
#include "args.h"
#include "icon_factory.h"
#include "menubar.h"
#include "toolbar.h"
#include "notebook.h"
#include "statusicon.h"
#include "statusbar.h"
#include "functions_ui.h"
#include "active_wifis.h"
#include "saved_wifis.h"
C_LINKAGE_BEGIN
#include <periodic_reminder.h>
#include <simple-netaid/sbuf.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/netproc.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>
C_LINKAGE_END

namespace SimpleNetaid
{
	
static time_t interval = 1;
static int stdinput, stdoutput, stderror;
	
#if GTK_MAJOR_VERSION == 3

static void
_paned_change_handle_size_green(GtkWidget *widget)
{
  GtkStyleContext * context = gtk_widget_get_style_context (widget);

  GtkCssProvider * css = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (css,
                                   "paned separator{"
                                   "background-size:3px;"
                                   "background-color:#5a7100;"
                                   "min-height: 6px;"
                                   "}",
                                   -1,NULL);


  gtk_style_context_add_provider (context, GTK_STYLE_PROVIDER(css), 
                      GTK_STYLE_PROVIDER_PRIORITY_USER);
}

#endif

static
char *read_link(const char *filename)
{
    char *buf;
    char *newbuf;
    size_t cap;
    ssize_t len;

    buf = NULL;
    for (cap = 64; cap <= 16384; cap *= 2) {
        newbuf = (char*)realloc(buf, cap);
        if (newbuf == NULL) {
            break;
        }
        buf = newbuf;
        len = readlink(filename, buf, cap);
        if (len < 0) {
            break;
        }
        if ((size_t)len < cap) {
            buf[len] = 0;
            return buf;
        }
    }
    free(buf);
    return NULL;
}

static
void child_ready_spawn_async_cb(VteTerminal *terminal,
                                GPid pid,
                                GError *error,
                                gpointer user_data)
{
	int *value = (int*)user_data;
	vte_terminal_watch_child (terminal, pid);
	
	Glib::ustring stdin = "/proc/" + std::to_string((int)pid) + "/fd/0";
	char *target = read_link(stdin.c_str());
	if(target) {
	    std::filesystem::path p(target);
	    *value = std::stoi(p.stem());
	    free(target);
	}
}

WindowMain::WindowMain(const Args& args)
 : // to register custom properties, we must register a custom GType by
   // adding a Glib::ObjectBase constructor call to this class' constructor
   Glib::ObjectBase(typeid(WindowMain)),
   ref(const_cast <Args&>(args)),
   prop_hidden(*this, "hidden_ok", false)
{	
	// Setup Window:
	set_title("simple-netaid");
	set_border_width(5);
	
	set_default_size(ref.get_width(), ref.get_height());
	
	if(ref.get_xpos()<0 || ref.get_ypos()<0)
        set_position(Gtk::WIN_POS_CENTER);
    else
        move(ref.get_xpos(), ref.get_ypos());
            
	set_default_icon_from_file(ref.get_app_icon ());
	//set_accept_focus(false);

	systray_ok = ref.get_systray();	
    property_hidden() = systray_ok;

	m_IconFactory = new IconFactory(args);
	
	Gtk::VBox *m_VBox = Gtk::manage(new Gtk::VBox());
    m_VBox->set_border_width(5);
	add(*m_VBox);
	
	m_ActiveWifis = new ActiveWifis(this, args);
	m_SavedWifis = new SavedWifis(this, args);	
	
	// MENUBAR:
	m_Menubar = new myMenuBar(this, args);
	m_VBox->pack_start(*m_Menubar, Gtk::PACK_SHRINK);
  
	// NOTEBOOK:
	m_Notebook = new myNotebook(this, m_ActiveWifis, m_SavedWifis, args);
	m_Notebook->set_border_width(10);
	
	// TOOLBAR:
	m_Toolbar = new myToolBar(this, m_ActiveWifis, args);

    // We created the notebook before the toolbar because the former is passed to the constructor of the latter,
    // but they are added to the gui the other way around
	m_VBox->pack_start(*m_Toolbar, Gtk::PACK_SHRINK);
    
    m_Pane1 = Gtk::manage(new Gtk::VPaned());    
    m_Pane2 = Gtk::manage(new Gtk::VPaned());
    
#if GTK_MAJOR_VERSION == 2

    m_Pane1->set_position(floor(ref.get_height()/2 + 10));
   
#elif GTK_MAJOR_VERSION == 3

    m_Pane1->set_position(floor(ref.get_height()/2));
    m_Pane2->set_position(floor(ref.get_height()/10));
    
#endif
	
	m_VBox->pack_start(*m_Pane1, Gtk::PACK_EXPAND_WIDGET, 0);
		
	std::vector<Gtk::VBox *> vbox;
	
	vbox.push_back(Gtk::manage(new Gtk::VBox()));
	m_Pane1->add(*vbox[0]);
	vbox[0]->pack_start(*m_Notebook);

	// STATUSBAR:
	m_Statusbar = new myStatusBar(this, args);
   
    GtkWidget *term1 = vte_terminal_new();
    GtkWidget *term2 = vte_terminal_new();  
   
    vte_terminal_set_scroll_on_output(VTE_TERMINAL(term1), TRUE);   
    vte_terminal_set_scroll_on_output(VTE_TERMINAL(term2), TRUE);
    
    gtk_widget_set_sensitive (term1, FALSE); 
    gtk_widget_set_sensitive (term2, FALSE);
    
    vte_terminal_set_size(VTE_TERMINAL(term1), 0, 0);
   
    Glib::ustring wired_on, wireless_on;
    ref.get_wired_on_cmd(wired_on);
    ref.get_wired_on_cmd(wireless_on);
    //Glib::spawn_command_line_sync ("ubus call ering.netaid start_periodic '{ \"interval\": 0 }'");
    Glib::spawn_command_line_sync (wired_on);   
    Glib::spawn_command_line_sync (wireless_on);
    
    Glib::ustring netlink_monitor_cmd;
    ref.get_netlink_monitor_cmd(netlink_monitor_cmd);
	
#if GTK_MAJOR_VERSION == 2

    const char *params[] = { "/bin/sh", "-c", netlink_monitor_cmd.c_str(), (char*)0 };
    const char *env[] = { "PATH=/usr/bin:/bin:./usr/local/bin:/bin", (char*)0 };     
    
    vte_terminal_fork_command (VTE_TERMINAL(term1), "sh", (char**)params, (char**)env, "~/", FALSE, FALSE, FALSE);
    
    const char *params1[] = { "/usr/bin/tail", "-f", (char*)0 };
        
    pid_t child_pid = vte_terminal_fork_command (VTE_TERMINAL(term2), "tail", (char**)params1, (char**)env, "~/", FALSE, FALSE, FALSE);
	
	Glib::ustring stdin = "/proc/" + std::to_string((int)child_pid) + "/fd/0";
	char *target = read_link(stdin.c_str());
	if(target) {
	    std::filesystem::path p(target);
	    ref.vte_tty = std::stoi(p.stem());
	    free(target);
	}

#elif GTK_MAJOR_VERSION == 3  
    
    Glib::ustring monitor = "sh -c 'sleep 5 && " + netlink_monitor_cmd + "'";    
    gchar **envp = NULL;
    gchar **command = NULL;
    g_shell_parse_argv (
         monitor.c_str(),           
         NULL,
         &command,
         NULL
    );
 
    vte_terminal_spawn_async (
         VTE_TERMINAL(term1),
         VTE_PTY_DEFAULT,
         NULL,
         command,
         NULL,
         (GSpawnFlags)0,  //(GSpawnFlags)(G_SPAWN_SEARCH_PATH |G_SPAWN_FILE_AND_ARGV_ZERO)
         NULL, NULL, NULL,
         -1,
         NULL, NULL, NULL
    );
 
    g_shell_parse_argv (
         "/usr/bin/tail -f",
         NULL,
         &command,
         NULL
    );

    vte_terminal_spawn_async (
         VTE_TERMINAL(term2),
         VTE_PTY_DEFAULT,
         NULL,
         command,
         NULL,
         (GSpawnFlags)0,  // (GSpawnFlags)(G_SPAWN_SEARCH_PATH |G_SPAWN_FILE_AND_ARGV_ZERO)
         NULL, NULL, NULL,
         -1,
         NULL,
         child_ready_spawn_async_cb,  // callback
         (void*)&ref.vte_tty
    );
		
#endif
    
	m_Pane1->add(*m_Pane2);
   
    vte_terminal_set_scroll_on_output(VTE_TERMINAL(term1), TRUE);
    Gtk::Widget *vte1 = Glib::wrap(term1);
    m_ScrolledWindow1 = Gtk::manage(new Gtk::ScrolledWindow());
    m_Pane2->add1(*m_ScrolledWindow1);
	m_ScrolledWindow1->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC); 
	m_ScrolledWindow1->add(*vte1);   
  
    vte_terminal_set_scroll_on_output(VTE_TERMINAL(term2), TRUE);    
    Gtk::Widget *vte2 = Glib::wrap(term2);
    m_ScrolledWindow2 = Gtk::manage(new Gtk::ScrolledWindow());
	m_Pane2->add2(*m_ScrolledWindow2);
	m_ScrolledWindow2->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC); 
	m_ScrolledWindow2->add(*vte2);

#if GTK_MAJOR_VERSION == 3
	
	_paned_change_handle_size_green(GTK_WIDGET(m_Pane2->gobj())); 

#endif
   
    m_VBox->pack_start(*m_Statusbar, Gtk::PACK_SHRINK);

	show_all_children();	
	
	start_periodic(interval);
	
	// The widget must be realized and mapped before grab_focus() is called.
	// That's why it's called after show_all_children().
	m_Notebook->grab_focus ();
   
    //Connect Server signals to the signal handlers in Client.
    update_connector = new sigc::connection
    (
       f_ui->signal_ui_update().connect(sigc::mem_fun(*this, &WindowMain::on_callback_ui_update))
    );
    f_ui->signal_ui_delete().connect(sigc::mem_fun(*this, &WindowMain::on_callback_ui_delete));   

    /* for signals received from netlink-monitor whenever the wire has been plugged in */
    f_ui->signal_ui_wired_connection().connect(sigc::mem_fun(*this, &WindowMain::on_callback_ui_wired_connection));
   
    update_connector->unblock();
            
    if(systray_ok) {
        
        set_skip_taskbar_hint(true);
    
        if(property_hidden()) {
            iconify();
            property_hidden() = true;
        }
            
        m_StatusIcon = new myStatusIcon(this, args); 
    }
}

WindowMain::~WindowMain()
{
	if(m_ActiveWifis) delete m_ActiveWifis;
	if(m_SavedWifis) delete m_SavedWifis;
	if(m_Menubar) delete m_Menubar;
	if(m_Toolbar) delete m_Toolbar;	
	if(m_Notebook) delete m_Notebook;
	if(m_Statusbar) delete m_Statusbar;
	if(m_IconFactory) delete m_IconFactory;
	if(systray_ok && m_StatusIcon) delete m_StatusIcon;

#if GTK_MAJOR_VERSION == 3

	delete f_ui;
	std::cout << "\033[1m\033[37m" << _("Exit gracefully") << "\033[0m" << std::endl;	
		
#endif
}

// Getter used by the statusicon
bool WindowMain::is_active_wifis_spinner_busy ()
{
    return m_ActiveWifis->is_spinner_busy ();	
}	

bool WindowMain::on_delete_event(GdkEventAny* /* event */)
{
	if (m_ActiveWifis->is_spinner_busy ()) {
	    return true;
    }
    
    if(systray_ok) {
        iconify();
        property_hidden()=true;
    } else {
        hide();
    }
    return true;
}

/*
  We cannot use C++ fuctions within the signal handler
  Instead, we make use of a C++ wrapper.

  Further info:
  - https://wiki.sei.cmu.edu/confluence/display/cplusplus/MSC54-CPP.+A+signal+handler+must+be+a+plain+old+function
*/
void WindowMain::on_callback_ui_update()
{
	update_connector->block();
	std::cout << _("**  Netlink event received  **\n") << std::endl;
	update_connector->unblock();
}

void WindowMain::on_callback_ui_delete()
{
	if (m_ActiveWifis->is_spinner_busy ()) {
	    return;
    }	
	hide();
}

void WindowMain::on_callback_ui_wireless_on()
{
	Glib::ustring cmd;
	ref.get_wireless_on_cmd (cmd);
	Glib::spawn_command_line_sync (cmd);
}

void WindowMain::on_callback_ui_wired_connection ()
{ 
    int pid;
    sigset_t mask;
	Glib::ustring cmd; 
    std::vector<std::string> argvproc;
  
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR2);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
        fprintf(stderr, "Cannot block SIGUSR2: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
 
	ref.get_wired_connection_cmd(cmd);
 
	argvproc.push_back ("/bin/sh");
	argvproc.push_back ("-c");
	argvproc.push_back (cmd.c_str());

    // Spawn the subprocess with pipes for input and output
    Glib::spawn_async_with_pipes ( Glib::get_current_dir(),                                    // Working directory
                                   argvproc,                                                   // Command to execute
                                   Glib::SPAWN_SEARCH_PATH | Glib::SPAWN_DO_NOT_REAP_CHILD,    // Spawn flags
                                   sigc::slot<void>(),                                         // Callback
                                   &pid,                                                       // Process ID
                                   &stdinput,                                                  // Standard input channel
                                   &stdoutput,                                                 // Standard output channel
                                   &stderror                                                   // Standard error channel
                                 );
                                  
    g_child_watch_add(pid, watch_pid, (gpointer)&mask);
}

void WindowMain::watch_pid (int pid, int status, gpointer user_data)
{
    Glib::spawn_close_pid (pid);
  
    if (status != 0) {
       /* error */
       std::cout << "Command failed" << std::endl;
    }
    else {
   	    
   	    sigset_t *mask = (sigset_t *)user_data;
   
        if (sigprocmask(SIG_UNBLOCK, mask, NULL) == -1) {
            fprintf(stderr, "Cannot unblock SIGUSR2: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
		
		sbuf_t s;
		bool connected_ok = false;             
        const char * ifname = iproute();
		if (*ifname != 0) connected_ok = true;
		ui_update_menubar (connected_ok);
		if (connected_ok) 
		   ui_update_toolbar (false);
		else 
		   ui_update_toolbar (true);
			 
		ui_update_statusbar (ifname);
			
        sbuf_init(&s);
        netproc(&s);
		ui_update_status_icon (s.buf);
		sbuf_free(&s);
    }
}

} // namespace SimpleNetaid
