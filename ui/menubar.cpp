/*
 * menubar.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <iostream>

#include "menubar.h"
#include "window_main.h"
#include "args.h"
#include "functions_ui.h"
#include <globals.h>
C_LINKAGE_BEGIN
#include <simple-netaid/sbuf.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/netproc.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>
C_LINKAGE_END

namespace SimpleNetaid
{

static int stdinput, stdoutput, stderror;

myMenuBar::myMenuBar(WindowMain *caller, const Args& args)
 : ref(const_cast <Args&>(args)) , IconFactory (args)
{  
	item = Gtk::manage(new Gtk::ImageMenuItem("Menu", true));
	append(*item);
	m_Submenu = Gtk::manage(new Gtk::Menu());
	item->set_submenu(*m_Submenu);
	
	m_WiredConnectionItem = Gtk::manage(new Gtk::ImageMenuItem(myMenuBar::WIRED_STOCK_ICON));
	m_Submenu->append(*m_WiredConnectionItem);
	m_WiredConnectionItem->signal_activate().connect
	(
	    sigc::mem_fun(*this, &myMenuBar::on_action_connect)
	);
	
	m_DisconnectItem = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::DISCONNECT));
	m_Submenu->append(*m_DisconnectItem);
	m_DisconnectItem->signal_activate().connect
	(
	    sigc::mem_fun(*this, &myMenuBar::on_action_disconnect)
	);
	
	m_QuitItem = Gtk::manage( new Gtk::ImageMenuItem(Gtk::Stock::QUIT));
	m_Submenu->append(*m_QuitItem);
	m_QuitItem->signal_activate().connect(
			sigc::mem_fun(*caller, &WindowMain::hide)
	);
	
	// Define the items of the second Menu:
	item = Gtk::manage(new Gtk::ImageMenuItem("Help", true));
	append(*item);
	m_Submenu = Gtk::manage(new Gtk::Menu());
	item->set_submenu(*m_Submenu);
	
	m_AboutItem = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::ABOUT));
	m_Submenu->append(*m_AboutItem);

	m_AboutItem->signal_activate().connect (
			sigc::mem_fun(*this, &myMenuBar::on_action_about)
	);

    update_menubar_connector = new sigc::connection
    (	
	   f_ui->signal_ui_update_menubar().connect(sigc::mem_fun (*this, &myMenuBar::on_callback_ui_update_menubar))
	);
	
	update_menubar_connector->unblock();
	
	m_Dialog.set_transient_for(*caller);
    m_Dialog.set_logo(Gdk::Pixbuf::create_from_file(ref.get_app_icon ()));
    m_Dialog.set_program_name("Simple Netaid");
    m_Dialog.set_version("1.0.5");
    m_Dialog.set_copyright("Aitor C.Z.");
    m_Dialog.set_comments(_("Gtk interface for simple-netaid"));
    m_Dialog.set_license("GPLv3");

    m_Dialog.set_website("http://www.gnuinos.org");
    m_Dialog.set_website_label(_("gnuinos website"));

    std::vector<Glib::ustring> list_authors;
    list_authors.push_back("- Aitor C.Z. (Gtk2/Gtk3 versions - 2024)");
    list_authors.push_back("- Edward Bartolo (Original version in C and Lazarus Free Pascal - 2015)");
    m_Dialog.set_authors(list_authors);
    
    m_Dialog.signal_response().connect(sigc::mem_fun(*this, &myMenuBar::on_about_dialog_response));
  
    show_all_children();    
}

myMenuBar::~myMenuBar()
{
}

void myMenuBar::on_callback_ui_update_menubar(bool connected_ok)
{
	update_menubar_connector->block();
	if(!connected_ok) {
		if (ethlink (ref.get_wired_device ())) m_WiredConnectionItem->set_sensitive(true);
		else m_WiredConnectionItem->set_sensitive(false);
		m_DisconnectItem->set_sensitive(false);
	}
	else {
		m_WiredConnectionItem->set_sensitive(false);
		m_DisconnectItem->set_sensitive(true);
	}
	update_menubar_connector->unblock();   
}

void myMenuBar::on_action_connect()
{
    /* Invoke WindowMain::on_callback_ui_wired_connection () */
    ui_wired_connection ();
}

void myMenuBar::on_action_disconnect()
{ 
    int pid;
    sigset_t mask;
	Glib::ustring cmd; 
    std::vector<std::string> argvproc;
  
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR2);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
        fprintf(stderr, "Cannot block SIGUSR2: %s.\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
 
	ref.get_disconnect_cmd(cmd);
 
	argvproc.push_back ("/bin/sh");
	argvproc.push_back ("-c");
	argvproc.push_back (cmd.c_str());

    // Spawn the subprocess with pipes for input and output
    Glib::spawn_async_with_pipes ( Glib::get_current_dir(),                                    // Working directory
                                   argvproc,                                                   // Command to execute
                                   Glib::SPAWN_SEARCH_PATH | Glib::SPAWN_DO_NOT_REAP_CHILD,    // Spawn flags
                                   sigc::slot<void>(),                                         // Callback
                                   &pid,                                                       // Process ID
                                   &stdinput,                                                  // Standard input channel
                                   &stdoutput,                                                 // Standard output channel
                                   &stderror                                                   // Standard error channel
                                 );
                                  
    g_child_watch_add(pid, watch_pid, (gpointer)&mask);
}

void myMenuBar::on_action_about()
{
	m_Dialog.show();

    //Bring it to the front, in case it was already shown:
    m_Dialog.present();
}

void myMenuBar::on_about_dialog_response(int response_id)
{
  std::cout << response_id
    << ", close=" << Gtk::RESPONSE_CLOSE
    << ", cancel=" << Gtk::RESPONSE_CANCEL
    << ", delete_event=" << Gtk::RESPONSE_DELETE_EVENT
    << std::endl;

  switch (response_id)
  {
  case Gtk::RESPONSE_CLOSE:
  case Gtk::RESPONSE_CANCEL:
  case Gtk::RESPONSE_DELETE_EVENT:
    m_Dialog.hide();
    break;
  default:
    std::cout << "Unexpected response!" << std::endl;
    break;
  }
}

void myMenuBar::watch_pid (int pid, int status, gpointer user_data)
{
    Glib::spawn_close_pid (pid);
  
    if (status != 0) {
       /* error */
       std::cout << "Command failed" << std::endl;
    }
    else {
   	    
   	    sigset_t *mask = (sigset_t *)user_data;
   
        if (sigprocmask(SIG_UNBLOCK, mask, NULL) == -1) {
            fprintf(stderr, "Cannot unblock SIGUSR2: %s.\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
		
		sbuf_t s;
		bool connected_ok = false;             
        const char * ifname = iproute();
		if (*ifname != 0) connected_ok = true;
		ui_update_menubar (connected_ok);
		if (connected_ok) 
		   ui_update_toolbar (false);
		else 
		   ui_update_toolbar (true);
			 
		ui_update_statusbar (ifname);
			
        sbuf_init(&s);
        netproc(&s);
		ui_update_status_icon (s.buf);
		sbuf_free(&s);
    }
}

} // namespace SimpleNetaid
