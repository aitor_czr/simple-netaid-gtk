/*
 * buttonbox.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include "buttonbox.h"

namespace SimpleNetaid
{

myButtonBox::myButtonBox(bool horizontal, int spacing, Gtk::ButtonBoxStyle layout)
{
	if (horizontal)
        bbox = new Gtk::HButtonBox;
    else
        bbox = new Gtk::VButtonBox;

    bbox->set_border_width(5);
    
    add(*bbox);
    bbox->set_layout(layout);
    bbox->set_spacing(spacing);
}

myButtonBox::~myButtonBox()
{

}

} // namespace SimpleNetaid
