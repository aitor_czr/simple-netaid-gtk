/*
 * popup.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include <gtkmm.h>
#include <iostream>

#include "popup.h"
#include "window_main.h"
#include "args.h"

namespace SimpleNetaid
{

myPopup::myPopup(WindowMain *caller, const Args& args)
{	
	m_Menu = new Gtk::Menu();
	
	checkItem = Gtk::manage(new Gtk::CheckMenuItem("Automatically connect", true));
	m_Menu->append(*checkItem);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::PREFERENCES));
	m_Menu->append(*item);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::INFO));
	m_Menu->append(*item);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::HELP));
	m_Menu->append(*item);
	
	hline = Gtk::manage(new Gtk::SeparatorMenuItem());
	m_Menu->append(*hline);
	
	item = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::QUIT));
	m_Menu->append(*item);
	item->signal_activate().connect (
		sigc::bind<WindowMain*>
		(
			sigc::mem_fun(*this, &myPopup::on_action_quit),
			caller
		)
	);
	
	m_Menu->show_all_children();
}

myPopup::~myPopup()
{
	if(m_Menu) delete m_Menu;
}

void myPopup::on_action_quit(WindowMain *caller)
{	
#if GTK_MAJOR_VERSION == 2 

	gtk_main_quit();

#elif GTK_MAJOR_VERSION == 3

	caller->hide();
		
#endif
}

void myPopup::show_popup_menu(guint button, guint32 activate_time)
{ 
	if(m_Menu)
		m_Menu->popup(button, activate_time);
}

} // namespace SimpleNetaid
