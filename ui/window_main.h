/*
 * window_main.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#ifndef __WINDOW_MAIN_H__
#define __WINDOW_MAIN_H__

#include <gtkmm.h>
#include <glibmm.h>
#include <vte/vte.h>

namespace SimpleNetaid
{

// Forward declarations:
class Args;
class IconFactory;
class myMenuBar;
class myToolBar;
class myNotebook;
class myStatusIcon;
class myStatusBar;
class ActiveWifis;
class SavedWifis;

// A class that contains properties must inherit from Glib::Object (or a class
// that inherits from Glib::Object)
class WindowMain : public Gtk::Window
{	
public:
	WindowMain(const Args&);
	virtual ~WindowMain();
	
	// Getter:
	bool is_active_wifis_spinner_busy ();
	
	// provide proxy for the properties.  The proxy allows us to connect to
    // the 'changed' signal, etc.
	Glib::PropertyProxy<bool> property_hidden() { return prop_hidden.get_proxy(); }
  
private:
    Args& ref;
    
	// Child widgets:
	Gtk::VPaned *m_Pane1, *m_Pane2;
	IconFactory *m_IconFactory;
	myMenuBar *m_Menubar;
	myToolBar *m_Toolbar;
	myNotebook *m_Notebook;
	myStatusIcon *m_StatusIcon;
	myStatusBar  *m_Statusbar;
	ActiveWifis *m_ActiveWifis;
	SavedWifis *m_SavedWifis;
	Gtk::ScrolledWindow *m_ScrolledWindow1, 
                        *m_ScrolledWindow2;

	bool systray_ok;
	Glib::Property<bool> prop_hidden;
   
    sigc::connection *update_connector;
		
	bool on_delete_event(GdkEventAny* /* event */);
   
    // Callbacks
    void on_callback_ui_update();
    void on_callback_ui_delete();
    void on_callback_ui_wired_connection();
    void on_callback_ui_wireless_on();

    void on_callback_vte2_tty_changed();
  
    static void watch_pid (int, int, gpointer);
	
	// Friend function - Statusicon (GTK+) callback:
	friend void on_statusicon_activated(GtkWidget*, gpointer);
};

} // namespace SimpleNetaid

#endif // __WINDOW_MAIN_H__
