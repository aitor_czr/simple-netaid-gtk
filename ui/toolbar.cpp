/*
 * toolbar.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <gtkmm.h>
#include <iostream>

#include "toolbar.h"
#include "window_main.h"
#include "args.h"
#include "notebook.h"
#include "active_wifis.h"
#include "functions_ui.h"
#include <globals.h>

namespace SimpleNetaid
{

myToolBar::myToolBar(WindowMain *caller, ActiveWifis * m_ActiveWifis, const Args& args)
 : ref(const_cast <Args&>(args)) , IconFactory (args)
{
	actionRefresh = Gtk::Action::create("edit position", myToolBar::REFRESH_STOCK_ICON, "Refresh", "Refresh the list of active wifis");	
	toolItem = actionRefresh->create_tool_item();
	this->append(*toolItem);
    
    actionRefresh->signal_activate().connect(sigc::mem_fun(*m_ActiveWifis, &ActiveWifis::on_action_iwlist_scanning));
	
	actionWiredConnection = Gtk::Action::create("edit game", myToolBar::WIRED_STOCK_ICON, "Wired", "Wired connection");	
	toolItem = actionWiredConnection->create_tool_item();
	this->append(*toolItem);
	
    actionWiredConnection->signal_activate().connect
	(
        sigc::bind<WindowMain*>
        (
           sigc::mem_fun(*this, &myToolBar::on_action_connect),
           caller
        )
    );
     		
	this->append(*(Gtk::manage(new Gtk::SeparatorToolItem)));	
		
	actionQuit = Gtk::Action::create("quit", Gtk::Stock::QUIT, "Quit", "Close the application");	
	toolItem = actionQuit->create_tool_item();
	this->append(*toolItem);	
    
    actionQuit->signal_activate().connect
	(
		sigc::mem_fun(*caller, &WindowMain::hide)
	);

    f_ui->signal_ui_update_toolbar().connect(sigc::mem_fun (*this, &myToolBar::on_callback_ui_update_toolbar));
}

myToolBar::~myToolBar()
{
}

void myToolBar::on_callback_ui_update_toolbar(bool wired_ok)
{
	if(wired_ok)
	   actionWiredConnection->set_sensitive(true);
	else
	   actionWiredConnection->set_sensitive(false);
}

void myToolBar::on_action_connect(WindowMain* caller)
{
	ui_wired_connection ();	
}

} // namespace SimpleNetaid
