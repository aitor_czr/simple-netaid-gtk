/*
 * toolbar.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __TOOLBAR_H__
#define __TOOLBAR_H__

#include "icon_factory.h"

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;
class ActiveWifis;

class myToolBar : public Gtk::Toolbar , public IconFactory
{    
public:
	myToolBar(WindowMain*, ActiveWifis*, const Args&);
	virtual ~myToolBar();

protected:
    Args& ref;
	
	// Child widgets:
	Gtk::ToolItem *toolItem;
	Glib::RefPtr<Gtk::Action> actionRefresh, actionWiredConnection, actionQuit;
	
	// Signal handlers:	
	virtual void on_callback_ui_update_toolbar(bool);
	
	virtual void on_action_connect(WindowMain*);
};

} // namespace SimpleNetaid

#endif // __TOOLBAR_H__
