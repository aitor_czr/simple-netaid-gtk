/*
 * args.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#include <iostream>
#include "args.h"
#include "INIReader.h"
	
extern "C" {
	
	#include <simple-netaid/sbuf.h>
	#include <simple-netaid/interfaces.h>

}

namespace SimpleNetaid
{

Args::Args(int argc, char **argv, const INIReader& reader, bool systray, int x, int y, int w, int h)
 : ref(const_cast <INIReader&>(reader))
{
	for(int i=0; i<argc; i++)
		argvs.push_back(Glib::ustring(argv[i]));
	
	this->m_systray = systray,
	this->xpos = x;
	this->ypos = y;
	this->width = w;
	this->height = h;
	
	/* devices */	
	//get_devices();    /* not required */
	WIRED_DEVICE = ref.Get("devices", "wired_device", "UNKNOWN");	
	WIRELESS_DEVICE = ref.Get("devices", "wireless_device", "UNKNOWN");

	get_pixmaps();
}

Args::~Args()
{
}

/* not required */
void Args::get_devices()
{
	struct sbuf wired_interface;
	struct sbuf wireless_interface;
	sbuf_init(&wired_interface);
	sbuf_init(&wireless_interface); 
	get_interfaces(&wired_interface, &wireless_interface); 
	WIRED_DEVICE = Glib::ustring(wired_interface.buf);
	WIRELESS_DEVICE = Glib::ustring(wireless_interface.buf);
	free(wired_interface.buf);
	free(wireless_interface.buf);	
}

void Args::get_pixmaps()
{
	Glib::ustring path;		
	Glib::ustring curr_dir = Glib::get_current_dir();
	Glib::ustring path_to_icons = ref.Get("paths", "path_to_icons", "UNKNOWN");

	Glib::RefPtr<Gio::File> ref_path_A = Gio::File::create_for_path(path_to_icons + "/SimpleNetaid.png");
	Glib::RefPtr<Gio::File> ref_path_B = Gio::File::create_for_path(curr_dir + "/images/SimpleNetaid.png");
	
	if ( ref_path_A->query_exists() )
		path = path_to_icons + "/";

	else if ( ref_path_B->query_exists() )
		path = curr_dir + "/images/";
  
	else std::cerr << " " << path << " doesn't exist! " << std::endl;
	
	APP_ICON = path + "SimpleNetaid.png";
	CONNECTED_ICON = path + "connected.png";
	DISCONNECTED_ICON = path + "disconnected.png";
	WIRED_ICON = path + "wired.png";
	WIRELESS_ICON = path + "wireless.png";
	VIEW_REFRESH_ICON = path + "view-refresh.png";
}

void Args::replace_variable (Glib::ustring& str, const Glib::ustring& from, const Glib::ustring& to)
{
   size_t idx = 0;
   
   while (1) {
     // find substring
     idx = str.find(from, idx);
     if (idx == Glib::ustring::npos) break;

     // relace it with target to
     str.replace(idx, from.length(), to);

     // jump further to after replacement
     idx += to.length();
   }
}

void Args::parse_command (Glib::ustring& cmd, Glib::ustring essid, Glib::ustring passwd, Glib::ustring filename)
{   
   replace_variable (cmd, "%e", WIRED_DEVICE);
   replace_variable (cmd, "%w", WIRELESS_DEVICE);
   replace_variable (cmd, "%n", std::to_string(vte_tty));
   
   replace_variable (cmd, "%s", essid);  
   replace_variable (cmd, "%p", passwd);  
   replace_variable (cmd, "%f", filename);
}

bool Args::get_systray ()
{
   return m_systray; 	
}

int Args::get_xpos ()
{
   return xpos; 	
}

int Args::get_ypos ()
{
   return ypos; 	
}

int Args::get_width ()
{
   return width; 	
}

int Args::get_height ()
{
   return height; 	
}

Glib::ustring Args::get_app_icon ()
{
   return APP_ICON;
}

Glib::ustring Args::get_connected_icon ()
{
   return CONNECTED_ICON;
}

Glib::ustring Args::get_disconnected_icon ()
{
   return DISCONNECTED_ICON;
}

Glib::ustring Args::get_view_refresh_icon ()
{
   return VIEW_REFRESH_ICON;
}

Glib::ustring Args::get_wired_icon ()
{
   return WIRED_ICON;
}

Glib::ustring Args::get_wireless_icon ()
{
   return WIRELESS_ICON;
}

const char * Args::get_wired_device () const
{
	return WIRED_DEVICE.c_str();
}

const char * Args::get_wireless_device () const
{
	return WIRELESS_DEVICE.c_str();
}

void Args::get_wired_on_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "wired_on", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_wired_off_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "wired_off", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_wireless_on_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "wireless_on", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_wireless_off_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "wireless_off", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_wired_connection_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "wired_connection", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_wireless_connection_cmd (Glib::ustring& cmd, Glib::ustring essid, Glib::ustring passwd, Glib::ustring filename)
{
	cmd = ref.Get("commands", "wireless_connection", "UNKNOWN");
	parse_command (cmd, essid, passwd, filename);
}

void Args::get_disconnect_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "disconnect", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_iwlist_scanning_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "iwlist_scanning", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

void Args::get_uninstall_cmd (Glib::ustring& cmd, Glib::ustring filename)
{
	cmd = ref.Get("commands", "uninstall", "UNKNOWN");
	parse_command (cmd, "", "", filename);
}

void Args::get_netlink_monitor_cmd (Glib::ustring& cmd)
{
	cmd = ref.Get("commands", "netlink_monitor", "UNKNOWN");
	parse_command (cmd, "", "", "");
}

} // namespace SimpleNetaid
