/*
 * notebook.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 
#include <iostream>

#include "notebook.h"
#include "window_main.h"
#include "args.h"
#include "buttonbox.h"
#include "active_wifis.h"
#include "saved_wifis.h"

namespace SimpleNetaid
{
	
// Constructor
myNotebook::myNotebook(WindowMain *caller, ActiveWifis * m_ActiveWifis, SavedWifis * m_SavedWifis, const Args& args)
:
  m_Label1("  Active Wifis  "),
  m_Label2("  Installed Wifis  ")
{
	set_scrollable(true);
	
	m_Align1.set_padding (0, 0, 0, 0);
	m_Align2.set_padding (0, 0, 0, 0);
  
	m_Align1.add (m_ScrolledWindow1);

	m_Align2.add (m_HBox);
	m_HBox.add (m_ScrolledWindow2);

	m_ScrolledWindow1.set_policy (Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);  
	m_ScrolledWindow2.set_policy (Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
	
	append_page (m_Align1, m_Label1);
	append_page (m_Align2, m_Label2);

	m_ScrolledWindow1.add (*m_ActiveWifis);
	m_ScrolledWindow2.add (*m_SavedWifis);
	
    m_HBox.pack_start (m_VBox2, Gtk::PACK_SHRINK);

    m_VBox2.set_border_width (10);

    m_VBox2.set_homogeneous (false);
    
    m_ButtonBox = new myButtonBox (false, 10, Gtk::BUTTONBOX_START);
    m_VBox2.pack_start (*m_ButtonBox, true, true, 5);
    
    m_ButtonConnect = Gtk::manage (new Gtk::Button (Gtk::Stock::CONNECT));
    m_ButtonBox->bbox->add(*m_ButtonConnect);
 
    m_ButtonRemove = Gtk::manage( new Gtk::Button ( Gtk::Stock::REMOVE));
    m_ButtonBox->bbox->add (*m_ButtonRemove);
	
	m_ButtonConnect->set_can_focus (false);
	m_ButtonRemove->set_can_focus (false);
	
	m_ButtonConnect->signal_clicked().connect (sigc::mem_fun( *m_SavedWifis, &SavedWifis::on_connect ));	
	m_ButtonRemove->signal_clicked().connect (sigc::mem_fun( *m_SavedWifis, &SavedWifis::on_remove ));
	
	m_ButtonConnect->set_can_focus(false);
	m_ButtonRemove->set_can_focus(false);

	show_all_children();
}

// Destructor (virtual)
myNotebook::~myNotebook()
{
	if(m_ButtonBox)   delete m_ButtonBox;
}

} // namespace SimpleNetaid
