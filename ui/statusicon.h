/*
 * statusicon.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __STATUSICON_H__
#define __STATUSICON_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;
class myPopup;

// Statusicon (GTK+) callbacks:
void on_statusicon_activated(GtkWidget*, gpointer);
void on_statusicon_popup(GtkStatusIcon*, guint, guint32, gpointer);

class myStatusIcon : public Gtk::StatusIcon
{    
public:
	myStatusIcon(WindowMain*, const Args&);
	virtual ~myStatusIcon();

private:
    Args& ref;
	std::string connected_icon, disconnected_icon;
	
	// Child widgets:
	myPopup *m_PopupMenu;
	Glib::RefPtr<Gtk::StatusIcon> m_refStatusIcon;
   
    sigc::connection *update_status_icon_connector;
	
	// Signal handlers:	
	virtual void on_callback_ui_update_status_icon(const char*);
	virtual void display_menu(guint, guint32);
	
	// Friend function - Statusicon (GTK+) callback:
	friend void on_statusicon_popup(GtkStatusIcon*, guint, guint32, gpointer);
};

} // namespace SimpleNetaid

#endif // __STATUSICON_H__
