/*
 * args.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */


#ifndef __ARGS_H__
#define __ARGS_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations
class INIReader;
	
class Args
{  
public:
	Args(int, char**, const INIReader&, bool, int, int, int, int);
	virtual ~Args();
	
	int vte_tty;

	virtual void get_devices ();
	virtual void get_pixmaps ();

	/* getters */
	bool get_systray ();
	int get_xpos ();
	int get_ypos ();
	int get_width ();
	int get_height ();
		
	Glib::ustring get_app_icon ();
	Glib::ustring get_connected_icon ();
	Glib::ustring get_disconnected_icon ();
	Glib::ustring get_view_refresh_icon ();
	Glib::ustring get_wired_icon ();
	Glib::ustring get_wireless_icon ();
	
	const char *get_wired_device () const;
	const char *get_wireless_device () const;
	void get_wired_on_cmd (Glib::ustring&);
	void get_wired_off_cmd (Glib::ustring&);
	void get_wireless_on_cmd (Glib::ustring&);
	void get_wireless_off_cmd (Glib::ustring&);
	void get_wired_connection_cmd (Glib::ustring&);
	void get_wireless_connection_cmd (Glib::ustring&, Glib::ustring, Glib::ustring, Glib::ustring);
	void get_disconnect_cmd (Glib::ustring&);
	void get_iwlist_scanning_cmd (Glib::ustring&);
	void get_uninstall_cmd (Glib::ustring&, Glib::ustring);
	void get_netlink_monitor_cmd (Glib::ustring&);
	
	void replace_variable (Glib::ustring&, const Glib::ustring&, const Glib::ustring&);
	void parse_command (Glib::ustring&, Glib::ustring, Glib::ustring, Glib::ustring);

private:
    INIReader& ref;	
	std::vector<Glib::ustring> argvs;
    
    bool m_systray;
    int xpos;
    int ypos;
    int width;
    int height;
    
	Glib::ustring CURR_DIR,
				  WIRED_DEVICE,
				  WIRELESS_DEVICE,
				  PATH_TO_ICONS,
				  APP_ICON,
				  CONNECTED_ICON,
				  DISCONNECTED_ICON,
				  WIRED_ICON,
				  WIRELESS_ICON,
				  VIEW_REFRESH_ICON;							
};

} // namespace SimpleNetaid

#endif // __ARGS_H__
