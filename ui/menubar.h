/*
 * menubar.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 

#ifndef __MENUBAR_H__
#define __MENUBAR_H__

#include <gtkmm.h>
#include "icon_factory.h"

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;

class myMenuBar : public Gtk::MenuBar , public IconFactory
{    
public:
	myMenuBar(WindowMain*, const Args&);
	virtual ~myMenuBar();

protected:
    Args& ref;	
	Gtk::Menu *m_Submenu;	
	Gtk::ImageMenuItem *item, *m_WiredConnectionItem, *m_DisconnectItem, *m_QuitItem, *m_AboutItem;
	Gtk::SeparatorMenuItem *hline;
	Gtk::AboutDialog m_Dialog;
   
    sigc::connection *update_menubar_connector;
	
	// Signal handlers:	
	virtual void on_callback_ui_update_menubar(bool);
	
	virtual void on_action_connect ();
	virtual void on_action_disconnect ();	
	virtual void on_action_about ();
  
    static void watch_pid (int, int, gpointer);

    virtual void on_about_dialog_response(int);
};

} // namespace SimpleNetaid

#endif // __MENUBAR_H__
