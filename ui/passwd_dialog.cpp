/*
 * passwd_dialog.cpp
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid-gtk is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "passwd_dialog.h"
#include "active_wifis.h"
#include "args.h"
#include "window_main.h" 
#include "buttonbox.h"
#include "saved_wifis.h"
#include "save_dialog.h"
#include <globals.h>
C_LINKAGE_BEGIN
#include <simple-netaid/iproute.h> 
C_LINKAGE_END

namespace SimpleNetaid
{

// Constructor
PasswdDialog::PasswdDialog (const int &action, Glib::ustring ssid, const Glib::ustring &passwd, const Glib::ustring &wifi_name, bool encryption)
 : ref_action(const_cast <int&>(action)),
   ref_passwd(const_cast <Glib::ustring&>(passwd)),
   ref_wifi_name(const_cast <Glib::ustring&>(wifi_name))
    
{
    ref_action = 0;   /* Do nothing */
    ref_passwd = Glib::ustring("");
    ref_wifi_name = Glib::ustring("");
    
    essid = ssid;
    is_encryption = encryption;
     
    set_size_request( 300, 250 );
  
    m_bActivityMode = true;
 
    set_title( "Password dialog" );
    set_position( Gtk::WIN_POS_CENTER );
  
    m_Box = Gtk::manage(new Gtk::VBox());

#if GTK_MAJOR_VERSION == 2  
    
    m_Box = Gtk::manage(new Gtk::VBox());

#elif GTK_MAJOR_VERSION == 3  
    
    m_Box = Gtk::manage(new Gtk::Box());
    m_Box = get_content_area();

#endif

    /* We static cast the child in order to avoid the warning: "Attempting to add a widget with type gtkmm__GtkVBox 
     * to a gtkmm__GtkDialog, but as a GtkBin subclass a gtkmm__GtkDialog can only contain one widget at a time; 
     * it already contains a widget of type GtkVBox".
     */
    auto dialog = static_cast<PasswdDialog*>(this->get_child());  
    dialog->add(*m_Box);
    
    m_Label = Gtk::manage(new Gtk::Label()); 
    m_Box->pack_start(*m_Label, Gtk::PACK_SHRINK, 5);
    m_Label->set_text( "\nEnter the password for " + essid + ":" );

#if GTK_MAJOR_VERSION == 3
  
    m_Label->set_vexpand(true);
    m_Label->set_hexpand(true);

#endif
 
    m_Entry = Gtk::manage(new Gtk::Entry());
    m_Box->pack_start(*m_Entry, Gtk::PACK_SHRINK, 5);

    m_Entry->set_visibility( false );  
    const Glib::ustring text= " Show password                ";
 
    bool mnemonic = false;  
    m_CheckButton_Visible = Gtk::manage(new Gtk::CheckButton( text, mnemonic ));
 
    m_Button_Connect = Gtk::manage(new Gtk::Button());
    m_Button_Connect_and_Save = Gtk::manage(new Gtk::Button());
 
    m_Box->add( *m_CheckButton_Visible ); 
    m_Box->add( *m_Button_Connect );
    m_Box->add( *m_Button_Connect_and_Save );

#if GTK_MAJOR_VERSION == 3
  
    m_CheckButton_Visible->set_margin_right(5);
    m_CheckButton_Visible->set_margin_left(20);
    m_CheckButton_Visible->set_margin_top(8);
    m_CheckButton_Visible->set_margin_bottom(15);

#endif
  
    m_Button_Connect->set_label ( "Connect"); 
    m_Button_Connect_and_Save->set_label ( "Save + Connect");

#if GTK_MAJOR_VERSION == 3
  
    m_Entry->set_margin_right(20);
    m_Entry->set_margin_left(20);
  
    m_Entry->select_region(0, m_Entry->get_text_length());
 
    m_Button_Connect->set_margin_right(25);
    m_Button_Connect->set_margin_left(25);
    m_Button_Connect->set_margin_top(1);
    m_Button_Connect->set_margin_bottom(6);
   
    m_Button_Connect_and_Save->set_margin_right(25);
    m_Button_Connect_and_Save->set_margin_left(25);
    m_Button_Connect_and_Save->set_margin_top(3);
    m_Button_Connect_and_Save->set_margin_bottom(10);

#endif

    m_CheckButton_Visible->set_active(false);
  	      
    m_CheckButton_Visible->signal_toggled().connect
    (
        sigc::mem_fun(*this, &PasswdDialog::on_checkbox_visibility_toggled)
    );
  
    m_Button_Connect->signal_clicked().connect
    (
        sigc::mem_fun( *this, &PasswdDialog::on_button_connect)
    );
    
    m_Button_Connect_and_Save->signal_clicked().connect
    (
        sigc::mem_fun (*this, &PasswdDialog::on_button_show_dialog)
    );
     
    m_Button_Connect->set_can_default();
    m_Button_Connect->grab_default();
    
    m_connection_timeout.unblock();

    show_all_children();
}

// Destructor
PasswdDialog::~PasswdDialog()
{

}

void PasswdDialog::on_checkbox_visibility_toggled()
{
  m_Entry->set_visibility( m_CheckButton_Visible->get_active() );
}

void PasswdDialog::on_button_connect()
{
	const char *ifname = iproute();
	
	if (*ifname != 0) {
	   static const char *info_message = _("\nAlready connected to %s\n");	
       GtkWidget *dialog = gtk_message_dialog_new (NULL,
                                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                                   GTK_MESSAGE_WARNING,
                                                   GTK_BUTTONS_OK,
                                                   info_message, ifname);
       gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid info message"));
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
       return;
    }

	if (is_encryption && m_Entry->get_text().size () == 0) {
	   static const char *info_message = _("\n%s is encrypted and requires password\n");	
       GtkWidget *dialog = gtk_message_dialog_new (NULL,
                                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                                   GTK_MESSAGE_WARNING,
                                                   GTK_BUTTONS_OK,
                                                   info_message, essid.c_str());
       gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid info message"));
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
       return;
    }
    
    ref_action = 1;
    ref_passwd = m_Entry->get_text ();
    ref_wifi_name.clear ();	
    hide();
}

void PasswdDialog::on_button_show_dialog()
{
	if (is_encryption && m_Entry->get_text().size () == 0) {
	   static const char *info_message = _("\n%s is encrypted and requires password\n");	
       GtkWidget *dialog = gtk_message_dialog_new (NULL,
                                                   GTK_DIALOG_DESTROY_WITH_PARENT,
                                                   GTK_MESSAGE_WARNING,
                                                   GTK_BUTTONS_OK,
                                                   info_message, essid.c_str());
       gtk_window_set_title(GTK_WINDOW(dialog), _("simple-netaid info message"));
       gtk_window_set_position (GTK_WINDOW(dialog), GTK_WIN_POS_MOUSE);
       gtk_dialog_run(GTK_DIALOG(dialog));
       gtk_widget_destroy(dialog);
       return;
    }
    
    ref_action = 0;
    SaveDialog *m_SaveDialog;
    m_SaveDialog = new SaveDialog (ref_action, ref_wifi_name);
    m_SaveDialog->run ();
    m_Entry->set_visibility (false);
    if (ref_action == 0) {
		ref_wifi_name.clear ();
		delete m_SaveDialog; 
        return;
    }
    /* Save + Connect */
    ref_action = 2;
    ref_passwd = m_Entry->get_text ();
    delete m_SaveDialog;
	hide ();
}

bool PasswdDialog::on_timeout()  /* unused */
{
   if(m_bActivityMode)
      m_ProgressBar.pulse();
  
   else
   {
      double new_val = m_ProgressBar.get_fraction() + 0.01;

      if(new_val > 1.0)
        new_val = 0.0;

      //Set the new value:
      m_ProgressBar.set_fraction(new_val);
   }
   return true;
}

} // namespace SimpleNetaid
