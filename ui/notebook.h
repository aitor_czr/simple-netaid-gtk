/*
 * notebook.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid-gtk is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */ 

#ifndef __NOTEBOOK_H__
#define __NOTEBOOK_H__

#include <gtkmm.h>

namespace SimpleNetaid
{

// Forward declarations:
class WindowMain;
class Args;
class myButtonBox;
class ActiveWifis;
class SavedWifis;

class myNotebook : public Gtk::Notebook
{
public:
	myNotebook(WindowMain*, ActiveWifis*, SavedWifis*, const Args&);
	virtual ~myNotebook();
	
protected:	
	std::string selected_addr;
	  
	// Child Widgets:	
	Gtk::HBox m_HBox;
	Gtk::VBox m_VBox1, m_VBox2;
	
	Gtk::ScrolledWindow m_ScrolledWindow1, 
                        m_ScrolledWindow2;
                        
	myButtonBox *m_ButtonBox;
  
	Gtk::Label     m_Label1, m_Label2;  
	Gtk::Alignment m_Align1, m_Align2;                    
	Gtk::Button    *m_ButtonConnect, *m_ButtonRemove;
	
	// Friend functions:
	friend void on_statusicon_activated(GtkWidget*, gpointer);
};

} // namespace SimpleNetaid

#endif // __NOTEBOOK_H__
