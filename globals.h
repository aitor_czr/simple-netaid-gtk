/*
 * globals.h
 * Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

  
#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifdef __cplusplus
#define C_LINKAGE_BEGIN extern "C" {
#define C_LINKAGE_END }
#else
#define C_LINKAGE_BEGIN 
#define C_LINKAGE_END
#endif

#include <stdbool.h>

struct functions_Ui;
typedef struct functions_Ui functions_Ui;
extern functions_Ui * f_ui;

/*====================== Functions the UI shall provide =====================*/
C_LINKAGE_BEGIN
void ui_update();
void ui_update_status_icon(const char*);
void ui_update_statusbar(const char*);
void ui_update_menubar(bool, bool);
void ui_update_toolbar(bool);
void ui_wired_connection();
void ui_delete();
C_LINKAGE_END

/*-------------------------- Global constant --------------------------------*/
extern const char *progname;

/*--------------------- Internationalization --------------------------------*/
#include <locale.h>
#include <libintl.h>
#define _(StRiNg)  dgettext("simple-netaid-gtk", StRiNg)

#endif  // __GLOBALS_H__
