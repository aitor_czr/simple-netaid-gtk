 /*
  * periodic_reminder.c
  * Copyright (C) 2019 Didier Kryn <kryn@in2p3.fr>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <globals.h>
#include "periodic_reminder.h"

void start_periodic(time_t interval)
/*-------------------------- Start periodic alarm -------------------------*/
{
  struct itimerval it_new = { {0, 0}, {0 ,0} };
  it_new.it_interval.tv_sec = interval;
  it_new.it_value.tv_sec = interval;
  
  if( setitimer(ITIMER_REAL, &it_new, NULL) )
    {
      fprintf( stderr, _("%s cannot start interval timer: %s\n"),
	       progname, strerror(errno) );
      exit(EXIT_FAILURE);
    }
}

void stop_periodic(void)
{
  struct itimerval it_new = { {0, 0}, {0 ,0} };
  if( setitimer(ITIMER_REAL, &it_new, NULL) )
    {
      fprintf( stderr, _("%s cannot start interval timer: %s\n"),
	       progname, strerror(errno) );
      exit(EXIT_FAILURE);
    }
}
