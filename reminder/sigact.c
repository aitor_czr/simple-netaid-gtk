 /*
  * sigact.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <errno.h>
#include <assert.h>
#include <sys/wait.h>
#include <dirent.h>
#include <signal.h>

#include <simple-netaid/sbuf.h>
#include <simple-netaid/ethlink.h>
#include <simple-netaid/netproc.h>
#include <simple-netaid/iproute.h>
#include <simple-netaid/interfaces.h>

#include "sigact.h"
#include "periodic_reminder.h"
#include <globals.h>

static int count = 1;
static volatile int active_wifis_interval = 1;
static volatile sig_atomic_t keep_going = 1;
static volatile sig_atomic_t operation_pending = 0;

/**
 * \brief Author: Didier Kryn (hopman) 
 * A subprogram like popen() but with diferences explained below
 * The pipe is to read in the current process and connected to stderr in the
 * new one. The intent is to report error messages. Plus pid is returned.
 * Disclaimer: The caller must close the pipe after use
 */
static
FILE *epopen( const char *cmd,  pid_t *pid)
{
  int pipefd[2];
  pid_t p_id;
  FILE *pf;
  int rc;

  // Create the pipe
  if(pipe(pipefd)) {
	  
      *pid = -1;
      return NULL;
  }

  p_id = fork();
 
  switch(p_id) {
    
    case 0: /* child */
      close(pipefd[0]);
      close(STDERR_FILENO);
      dup2(pipefd[1], STDERR_FILENO);
      close(pipefd[1]);
      execl( "/bin/sh", "sh", "-c", cmd, (char *)NULL );
      fprintf( stderr, _("%s: error invoking /bin/sh: %s\n"), progname, strerror(errno) );
      exit(EXIT_FAILURE); /* reminder: we are the child */
    
    case -1: /* fork() failed ! */
      rc = errno;
      close(pipefd[0]);
      close(pipefd[1]);
      errno = rc;
      *pid = -1;
      return NULL;
    
    default: /* the parent */
      close(pipefd[1]);
      pf = fdopen(pipefd[0], "r");
      *pid = p_id;
      return pf;
  }
}

static
void hdl (int signum, siginfo_t *info, void *extra)
{
   switch (signum)
   {
      case SIGALRM:
		 if (keep_going)
         {
			 bool connected_ok = false;
			 bool wired_ok = false;
			 //bool wireless_ok = false;
			 
			 sbuf_t s;
             sbuf_t wired_device, wireless_device;
             
             const char * ifname = iproute();
             
	         sbuf_init(&wired_device);
	         sbuf_init(&wireless_device);	
	         get_interfaces(&wired_device, &wireless_device);
			 
			 if (*ifname != 0) connected_ok = true;
	         if (ethlink(wired_device.buf) > 0) wired_ok = true;
	         //if (get_interface_status(wireless_device.buf)) wireless_ok = true;
			 ui_update_menubar (connected_ok, wired_ok);
			 if (connected_ok) 
			    ui_update_toolbar (false);
			 else 
			    ui_update_toolbar (true);
			 
			 ui_update_statusbar (ifname);      
	         
	         sbuf_free(&wired_device);
	         sbuf_free(&wireless_device);
			
             sbuf_init(&s);
             netproc(&s);
			 ui_update_status_icon (s.buf);
			 sbuf_free(&s);
		     keep_going = 0;
	     }
	     if (active_wifis_interval>0) {
			 if (count % active_wifis_interval == 0) {
	             ui_update();
			 }    
			 count++;    
		 }	     
         break;
      case SIGCHLD:
         break;
      case SIGHUP:
         break;
      case SIGUSR1:
         { 
			 int rc_int_val = info->si_value.sival_int;
			 if (rc_int_val == 1)
			     ui_wired_connection();
         }
         break;
      case SIGUSR2:
         {
             keep_going = 1;
             if(operation_pending == 1) return;
             operation_pending = 1;
			 int rc_int_val = info->si_value.sival_int;
			 /*
			 if (rc_int_val == 1) {
				 FILE *pfin = NULL;
                 pid_t pid;
                 int wstatus = -1;
                 sbuf_t s __cleanbuf__;
                 sbuf_init(&s);   
                 sbuf_addstr(&s, "ubus call ering.netaid start_periodic '{ \"interval\": 0 }'");
                 pfin = epopen(s.buf, &pid);
                 if(pfin) { 
                    fclose(pfin);
                    waitpid(pid, &wstatus, 0);
                 }
                 sbuf_free(&s);
			 }
			 else
			 */
			 if (rc_int_val == 2) {
				 FILE *pfin = NULL;
                 pid_t pid;
                 int wstatus = -1;
                 sbuf_t s __cleanbuf__;
                 sbuf_t wired_device __cleanbuf__;
                 sbuf_t wireless_device __cleanbuf__;
             
	             sbuf_init(&wired_device);
	             sbuf_init(&wireless_device);	
	             get_interfaces(&wired_device, &wireless_device);
                 sbuf_init(&s);   
                 sbuf_concat(&s, 3, "ubus call ering.netaid interface_up '{ \"ifname\": \"", wireless_device.buf, "\", \"tty\": 0 }'");
                 pfin = epopen(s.buf, &pid);
                 if(pfin) { 
                    fclose(pfin);
                    waitpid(pid, &wstatus, 0);
                 }
                 sbuf_free(&s);
                 sbuf_free(&wired_device);
	             sbuf_free(&wireless_device);
			 }
			 else if (rc_int_val == 3) {
				 FILE *pfin = NULL;
                 pid_t pid;
                 int wstatus = -1;
                 sbuf_t s __cleanbuf__;
                 sbuf_t wired_device __cleanbuf__;
                 sbuf_t wireless_device __cleanbuf__;
             
	             sbuf_init(&wired_device);
	             sbuf_init(&wireless_device);	
	             get_interfaces(&wired_device, &wireless_device);
                 sbuf_init(&s);   
                 sbuf_concat(&s, 3, "ubus call ering.netaid wired_connection '{ \"ifname\": \"", wired_device.buf, "\", \"tty\": 0 }'");
                 pfin = epopen(s.buf, &pid);
                 if(pfin) { 
                    fclose(pfin);
                    waitpid(pid, &wstatus, 0);
                 }
                 sbuf_free(&s);
                 sbuf_free(&wired_device);
	             sbuf_free(&wireless_device);
			 }
			 else if (rc_int_val < 0 || rc_int_val > 3)
			     active_wifis_interval = rc_int_val;

             operation_pending = 0;
         }
         break;
      case SIGTERM:
      case SIGINT:
      case SIGQUIT:
         stop_periodic (); 
         /* Make sure to cleanup if user sends a keyboard interrupt */
         ui_delete ();
         break;
   }
}

void sigaction_init ()
{
   struct sigaction sa;
   memset (&sa, 0, sizeof(sa));
      
   /*---------------- initialize signal handling ------------ */
   sa.sa_sigaction = &hdl;
   sa.sa_flags = SA_RESTART | SA_NODEFER | SA_SIGINFO; //  SA_RESETHAND | 
   sigemptyset (&sa.sa_mask);
   sigaction (SIGALRM, &sa, 0);
   sigaction (SIGCHLD, &sa, 0);
   sigaction (SIGHUP,  &sa, 0);
   sigaction (SIGUSR1, &sa, 0);
   sigaction (SIGUSR2, &sa, 0);
   sigaction (SIGTERM, &sa, 0);
   sigaction (SIGINT,  &sa, 0);
   sigaction (SIGQUIT, &sa, 0);
}
