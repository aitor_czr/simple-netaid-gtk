 /*
  * periodic_reminder.h
  * Copyright (C) 2019 Didier Kryn <kryn@in2p3.fr>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  
  
#ifndef __PERIODIC_REMINDER_H__
#define __PERIODIC_REMINDER_H__

void start_periodic(time_t);
void stop_periodic(void);

#endif  //  __PERIODIC_REMINDER_H__
