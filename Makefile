
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH 2>/dev/null || true)

UI_DIR   := ui
REMINDER_DIR := reminder

VPATH := $(UI_DIR):$(REMINDER_DIR)

DESTDIR ?=
PREFIX ?= /usr/local
BINDIR ?= $(DESTDIR)$(PREFIX)/bin

GTK_VERSION    ?= 2

PROG := $(patsubst %, %$(shell echo $(GTK_VERSION)), simple-netaid-gtk)

UI_DEPS  := $(shell $(MAKE) -s -r -C $(UI_DIR) print_deps)
REMINDER_DEPS := $(shell $(MAKE) -s -r -C $(REMINDER_DIR) print_deps)

INSTALLED_ICONS = $(DESTDIR)$(PREFIX)/share/pixmaps/SimpleNetaid.png $(DESTDIR)$(PREFIX)/share/simple-netaid/connected.png $(DESTDIR)$(PREFIX)/share/simple-netaid/disconnected.png \
$(DESTDIR)$(PREFIX)/share/simple-netaid/view-refresh.png $(DESTDIR)$(PREFIX)/share/simple-netaid/wired.png $(DESTDIR)$(PREFIX)/share/simple-netaid/wireless.png \
$(DESTDIR)$(PREFIX)/share/simple-netaid/SimpleNetaid.png

INSTALLED_FILES = $(BINDIR)/$(PROG) $(DESTDIR)/etc/simple-netaid/gtk.ini $(DESTDIR)$(PREFIX)/share/applications/simple-netaid-gtk.desktop $(INSTALLED_ICONS)

.SUFFIXES:

all: simple-netaid-gtk

simple-netaid-gtk: $(UI_DEPS)
	@$(MAKE) -r -C $(UI_DIR) $(PROG)

$(REMINDER_DIR)/libreminder.a: $(REMINDER_DEPS)
	@$(MAKE) -r -C $(REMINDER_DIR) libreminder.a

install: $(INSTALLED_FILES)

$(BINDIR)/$(PROG): simple-netaid-gtk
	cp -av $< $@

$(DESTDIR)/etc/simple-netaid/gtk.ini: config/gtk.ini
	cp $< $@

$(DESTDIR)$(PREFIX)/share/applications/simple-netaid-gtk.desktop: simple-netaid-gtk.desktop
	cp $< $@

$(DESTDIR)$(PREFIX)/share/pixmaps/SimpleNetaid.png: images/SimpleNetaid.png
	cp $< $@

$(DESTDIR)$(PREFIX)/share/simple-netaid/connected.png: images/connected.png
	cp $< $@

$(DESTDIR)$(PREFIX)/share/simple-netaid/disconnected.png: images/disconnected.png
	cp $< $@

$(DESTDIR)$(PREFIX)/share/simple-netaid/view-refresh.png: images/view-refresh.png
	cp $< $@

$(DESTDIR)$(PREFIX)/share/simple-netaid/wired.png: images/wired.png
	cp $< $@

$(DESTDIR)$(PREFIX)/share/simple-netaid/wireless.png: images/wireless.png
	cp $< $@

$(DESTDIR)$(PREFIX)/share/simple-netaid/SimpleNetaid.png: images/SimpleNetaid.png
	cp $< $@

clean:
	$(MAKE) -r -C $(REMINDER_DIR) clean
	$(MAKE) -r -C $(UI_DIR) clean

cleanall:
	$(MAKE) -r -C $(REMINDER_DIR) cleanall
	$(MAKE) -r -C $(UI_DIR) cleanall
	rm -vf $(wildcard *~)

uninstall:
	rm -vf $(INSTALLED_FILES)

.PHONY: clean cleanall uninstall
.SILENT: clean cleanall uninstall
